// var accessToken = null;
// var refreshToken = null;
// var idToken = null;
var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";
var projectID = localStorage.getItem("currentProjectID");
// let projectID = "Project#baolong#project4";
var projectInfo = null;
var projectServers = [];
var projectIssues = [];
var projectMembers = [];
var ownWorkingProjectItem = null;
var username = null;
var chosenMemberArray = [];
var newMembersArray = [];

function getPastelColor(){ 
    return "hsl(" + 360 * Math.random() + ',' +
               (25 + 70 * Math.random()) + '%,' + 
               (85 + 10 * Math.random()) + '%)'
}

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

function redirectToDomain(domain) {
    window.location.href = domain;
}

function setUserInfo(userInfos) {
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = userInfos["UserAttributes"];
        if(userAttributes.hasOwnProperty("picture")) {
            $(".avatar").attr("src", userAttributes["picture"]);
        }
    }
}

function getUserInfo() {
    let userInfos = {
        "Username": localStorage.getItem("username"),
        "UserAttributes": JSON.parse(localStorage.getItem("userAttributes"))
    }
    username = userInfos["Username"];
    setUserInfo(userInfos);
}

function getEverythingAboutProject(projectID, callback) {
    $.ajax({
        url: baseURL + "/project/get-everything-about-project",
        type: "GET",
        data: {
            "projectID": projectID
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function getProject(callback) {
    getEverythingAboutProject(projectID, function(everything) {
        let items = everything["Items"];
        items.forEach((item, idx, arr) => {
            if(item["SK"][0] == "S") {
                projectServers.push(item);
            }
            else if(item["SK"][0] == "I") {
                projectIssues.push(item);
            }
            else if(item["SK"][0] == "P") {
                if(item["PK"][0] == "P") {
                    projectInfo = item;
                }
                else if(item["PK"][0] == "U") {
                    projectMembers.push(item);
                    if(item["PK"].split("#")[1] == username) {
                        ownWorkingProjectItem = item;
                    }
                }
            }
            if(idx == arr.length - 1) {
                callback();
            }
        })
    })
}

function fillProjectSummary() {
    $(".project-avatar").html(projectInfo["PK"].split("#")[2][0].toUpperCase()).css("background-color", getPastelColor());
    $(".project-summary-holder h1").html(projectInfo["PK"]);
    $("#projectSummaryAuthor").html(projectInfo["PK"].split("#")[1]);
    $("#projectSummaryRole").html(ownWorkingProjectItem["Role"]);
    $("#projectSummaryJoin").html(ownWorkingProjectItem["Joined"]);
    $("#projectSummaryStar").html(ownWorkingProjectItem["Starred"].toString());
}

function fillProject() {
    $("#projectName").val(projectInfo["PK"].split("#")[2]);
    $("#projectCreator").val(projectInfo["PK"].split("#")[1]);
    $("#projectDate").val(projectInfo["DayInit"]);
    $("#projectDescription").val(projectInfo["Description"]);
    $("#projectCodebase").val(projectInfo["Codebase"]);
    if(projectInfo["Codebase"] == "New") {
        $("#project-view-row-codebaseSource").css("display", "none");
    }
    else if(projectInfo["Codebase"] == "Git") {
        $("#projectCodebaseSource").val(projectInfo["CodebaseSource"]);
    }
    else if(projectInfo["Codebase"] == "Upload") {
        $("#projectCodebaseSource").val("Download");
    }
    if(ownWorkingProjectItem["Role"] == "Owner") {
        $("#projectRole").prop('disabled', 'disabled').append(new Option("Owner", "Owner"));
    }
    $("#projectRole").val(ownWorkingProjectItem["Role"]);
    $("#projectJoin").val(ownWorkingProjectItem["Joined"]);
    $("#projectStar").prop("checked", ownWorkingProjectItem["Starred"]);
}

function fillIssues() {
    projectIssues.forEach(issue => {
        $("#project-view-issues").append(
            $("<div>", {class:"issue-view-holder", id: "issue-view-holder-" + issue["SK"].split("#")[1]}).append([
                $("<h2>", {class: "issue-title", text: issue["Title"], id: "issue-title-" + issue["SK"].split("#")[1]}),
                $("<select>", {class: "issue-status", id: "issue-status-" + issue["SK"].split("#")[1]}).append([
                    new Option("To do", "To do", true, "To do" == issue["Status"]),
                    new Option("Doing", "Doing", false, "Doing" == issue["Status"]),
                    new Option("Done", "Done", false, "Done" == issue["Status"])
                ]),
                $("<p>", {class: "issue-init", id: "issue-init-" + issue["SK"].split("#")[1]}).append([
                    "Created: ",
                    $("<span>", {text: issue["InitDay"]})
                ]),
                $("<p>", {class: "issue-deadline", id: "issue-deadline-" + issue["SK"].split("#")[1]}).append([
                    "Deadline: ",
                    $("<span>", {text: issue["Deadline"]})
                ]),
                $("<p>", {class: "issue-creator", id: "issue-creator-" + issue["SK"].split("#")[1]}).append([
                    "Creator: ",
                    $("<span>", {text: issue["Create-PK"].split("#")[1]})
                ]),
                $("<p>", {class: "issue-description", id: "issue-description-" + issue["SK"].split("#")[1]}).html(issue["Description"]),
                $("<p>", {class: "issue-urgency", id: "issue-urgency-" + issue["SK"].split("#")[1]}).append([
                    "Urgency: ",
                    $("<span>", {text: issue["Urgency"]})
                ]),
                $("<div>", {class: "issue-assign", id: "issue-assign-" + issue["SK"].split("#")[1]}).append([
                    // $("<input>", {type:"text", class:"issue-assign-input", value: issue["Assign-PK"], id: "issue-assign-input-" + issue["SK"].split("#")[1]}),
                    function() {
                        let assignee = "";
                        if(issue["Assign-PK"] != "NULL") {
                            assignee = issue["Assign-PK"];
                        }
                        return $("<input>", {type:"text", class:"issue-assign-input", value: assignee, id: "issue-assign-input-" + issue["SK"].split("#")[1]})
                    },
                    $("<div>", {class: "issue-assign-recommend-table", id: "issue-assign-recommend-table-" + issue["SK"].split("#")[1]}).html(
                        $("<ul>", {class: "issue-assign-recommend-list", id: "issue-assign-recommend-list-" + issue["SK"].split("#")[1]})
                    )
                ]),
                $("<div>", {class: "issue-buttons"}).append([
                    $("<button>", {class: "issue-button-action btn", id: "issue-button-action-" + issue["SK"].split("#")[1]}).append([
                        "Action ",
                        $("<i>", {class: "fas fa-caret-down"})
                    ]).click(function() {
                        $("#issue-button-action-" + issue["SK"].split("#")[1]).css("background-color", "rgb(246, 76, 114)");
                        if($("#issue-button-action-list-" + issue["SK"].split("#")[1]).css("display") == "none")
                            $("#issue-button-action-list-" + issue["SK"].split("#")[1]).css("display", "block");
                        else 
                            $("#issue-button-action-list-" + issue["SK"].split("#")[1]).css("display", "none");

                    }),
                    $("<div>", {class: "issue-button-action-list", id: "issue-button-action-list-" + issue["SK"].split("#")[1]}).append([
                        $("<button>", {class: "btn issue-button-save", text: "Save"}).click(function() {
                            $("#issue-button-action-list-" + issue["SK"].split("#")[1]).css("display", "none");
                            let newIssueStatus = $("#issue-status-" + issue["SK"].split("#")[1]).val();
                            let newIssueAssign = $("#issue-assign-input-" + issue["SK"].split("#")[1]).val();
                            if(newIssueStatus != issue["Status"]) {
                                issue["Status"] = newIssueStatus;
                                updateIssueStatus(issue["SK"], {
                                    "Status": newIssueStatus
                                }, function() {
                                    if(newIssueAssign != issue["Assign-PK"]) {
                                        issue["Assign-PK"] = newIssueAssign;
                                        assignIssue(issue["SK"], newIssueAssign.split("#")[1], function() {
                                            $("#issue-button-action-" + issue["SK"].split("#")[1]).css("background-color", "green");
                                        })
                                    }
                                    else {
                                        $("#issue-button-action-" + issue["SK"].split("#")[1]).css("background-color", "green");
                                    }
                                });
                            }
                            else {
                                if(newIssueAssign != issue["Assign-PK"]) {
                                    issue["Assign-PK"] = newIssueAssign;
                                    assignIssue(issue["SK"], newIssueAssign.split("#")[1], function() {
                                        $("#issue-button-action-" + issue["SK"].split("#")[1]).css("background-color", "green");
                                    })
                                }
                            }
                        }),
                        $("<button>", {class: "btn issue-button-delete", text: "Delete"}).click(function() {
                            $("#issue-button-action-list-" + issue["SK"].split("#")[1]).css("display", "none");
                            deleteIssue(issue["SK"], function(response) {
                                $("#issue-view-holder-" + issue["SK"].split("#")[1]).remove();
                                const idx = projectIssues.indexOf(issue);
                                if (idx > -1) {
                                    projectIssues.splice(idx, 1);
                                }
                                console.log(response);
                            })
                        })
                    ])
                ])


                // $("<button>", {class: "issue-buttons btn", text: "Save", id: "issue-buttons-" + issue["SK"].split("#")[1]}).click(function() {
                //     $("#issue-buttons-" + issue["SK"].split("#")[1]).css("background-color", "rgb(246, 76, 114)");
                //     let newIssueStatus = $("#issue-status-" + issue["SK"].split("#")[1]).val();
                //     let newIssueAssign = $("#issue-assign-input-" + issue["SK"].split("#")[1]).val();
                //     if(newIssueStatus != issue["Status"]) {
                //         issue["Status"] = newIssueStatus;
                //         updateIssueStatus(issue["SK"], {
                //             "Status": newIssueStatus
                //         }, function() {
                //             if(newIssueAssign != issue["Assign-PK"]) {
                //                 issue["Assign-PK"] = newIssueAssign;
                //                 assignIssue(issue["SK"], newIssueAssign.split("#")[1], function() {
                //                     $("#issue-buttons-" + issue["SK"].split("#")[1]).css("background-color", "green");
                //                 })
                //             }
                //             else {
                //                 $("#issue-buttons-" + issue["SK"].split("#")[1]).css("background-color", "green");
                //             }
                //         });
                //     }
                //     else {
                //         if(newIssueAssign != issue["Assign-PK"]) {
                //             issue["Assign-PK"] = newIssueAssign;
                //             assignIssue(issue["SK"], newIssueAssign.split("#")[1], function() {
                //                 $("#issue-buttons-" + issue["SK"].split("#")[1]).css("background-color", "green");
                //             })
                //         }
                //     }
                // })
            ])
        )
    })
}

function getMemberInfo(memberName, callback) {
    $.ajax({
        url: baseURL + "/user/get-public-user-details",
        type: "POST",
        data: JSON.stringify({
            "targetUsername": memberName
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function memberRoleToJquery(role, username) {
    if(role == "Owner") {
        return $("<select>", {name: "memberRole", id: "memberRole" + username}).prop("disabled", "disabled").html(
            new Option("Owner", "Owner", true)
        )
    } else {
        return $("<select>", {name: "memberRole", id: "memberRole" + username}).append([
            new Option("Maintainer", "Maintainer", true, "Maintainer" == role),
            new Option("Developer", "Developer", false, "Developer" == role),
            new Option("Guest", "Guest", false, "Guest" == role)
        ])
    }
}

function memberRemoveToJquery(member) {
    if(member["Role"] != "Owner") {
        return $("<button>", {class: "member-view-button-remove btn", text: "Remove"}).click(function() {
            removeMember(member["PK"].split("#")[1], function() {
                $("#memberView" + member["PK"].split("#")[1]).remove();
                const index = projectMembers.indexOf(member);
                if (index > -1) {
                    projectMembers.splice(index, 1);
                }
                const idx = chosenMemberArray.indexOf(member["PK"]);
                if (idx > -1) {
                    chosenMemberArray.splice(idx, 1);
                }
            });
        })
    }
}

function addMemberList(member) {
    chosenMemberArray.push(member["PK"]);
    getMemberInfo(member["PK"].split("#")[1], function(response) {
        let memberAttributes = {};
        response["UserAttributes"].forEach((attr, idx, arr) =>{
            memberAttributes[attr["Name"]] = attr["Value"];
            if(idx == arr.length - 1) {
                if(!memberAttributes.hasOwnProperty("picture")) {
                    memberAttributes["picture"] = "img/avatar-default.png";
                }
                $(".member-view-list").append([
                    $("<div>", {class: "member-view", id: "memberView" + member["PK"].split("#")[1]}).append([
                        $("<div>", {class: "member-view-avatar col span-1-of-6"}).html(
                            $("<img>", {src: memberAttributes["picture"], alt:"avatar"})
                        ),
                        $("<div>", {class: "member-view-info col span-4-of-6"}).append([
                            $("<div>", {class: "member-view-info-upper"}).html(
                                $("<h2>", {text: member["PK"]})
                            ),
                            $("<div>", {class: "member-view-info-lower"}).append([
                                $("<div>", {class: "member-view-info-role col span-1-of-2"}).append([
                                    $("<label>", {for: "role", text: "Role: "}),
                                    memberRoleToJquery(member["Role"], member["PK"].split("#")[1])
                                ]),
                                $("<div>", {class: "member-view-info-join col span-1-of-2"}).append([
                                    $("<label>", {for: "join", text: "Joined on: "}),
                                    $("<span>", {text: member["Joined"]})
                                ])
                            ])
                        ]),
                        $("<div>", {class: "member-view-button col span-1-of-6"}).append([
                            $("<button>", {class: "member-view-button-update btn", text: "Update", id: "memberViewButtonUpdate" + member["PK"].split("#")[1]}).click(function() {
                                $(this).css("background-color", "rgb(246, 76, 114)");
                                let newMemberRole = $("#memberRole" + member["PK"].split("#")[1]).val();
                                if(newMemberRole != member["Role"]) {
                                    member["Role"] = newMemberRole;
                                    updateWorkingInfo(member["PK"].split("#")[1], {
                                        "Role": newMemberRole
                                    }, function() {
                                        $("#memberViewButtonUpdate" + member["PK"].split("#")[1]).css("background-color", "green");
                                    })
                                }
                            }),
                            memberRemoveToJquery(member)
                        ])
                    ])
                ])
            }
        });
    });
}

function fillMembers() {

    projectMembers.forEach(member => {
        chosenMemberArray.push(member["PK"]);
        getMemberInfo(member["PK"].split("#")[1], function(response) {
            let memberAttributes = {};
            response["UserAttributes"].forEach((attr, idx, arr) =>{
                memberAttributes[attr["Name"]] = attr["Value"];
                if(idx == arr.length - 1) {
                    if(!memberAttributes.hasOwnProperty("picture")) {
                        memberAttributes["picture"] = "img/avatar-default.png";
                    }
                    $(".member-view-list").append([
                        $("<div>", {class: "member-view", id: "memberView" + member["PK"].split("#")[1]}).append([
                            $("<div>", {class: "member-view-avatar col span-1-of-6"}).html(
                                $("<img>", {src: memberAttributes["picture"], alt:"avatar"})
                            ),
                            $("<div>", {class: "member-view-info col span-4-of-6"}).append([
                                $("<div>", {class: "member-view-info-upper"}).html(
                                    $("<h2>", {text: member["PK"]})
                                ),
                                $("<div>", {class: "member-view-info-lower"}).append([
                                    $("<div>", {class: "member-view-info-role col span-1-of-2"}).append([
                                        $("<label>", {for: "role", text: "Role: "}),
                                        memberRoleToJquery(member["Role"], member["PK"].split("#")[1])
                                    ]),
                                    $("<div>", {class: "member-view-info-join col span-1-of-2"}).append([
                                        $("<label>", {for: "join", text: "Joined on: "}),
                                        $("<span>", {text: member["Joined"]})
                                    ])
                                ])
                            ]),
                            $("<div>", {class: "member-view-button col span-1-of-6"}).append([
                                $("<button>", {class: "member-view-button-update btn", text: "Update", id: "memberViewButtonUpdate" + member["PK"].split("#")[1]}).click(function() {
                                    $(this).css("background-color", "rgb(246, 76, 114)");
                                    let newMemberRole = $("#memberRole" + member["PK"].split("#")[1]).val();
                                    if(newMemberRole != member["Role"]) {
                                        member["Role"] = newMemberRole;
                                        updateWorkingInfo(member["PK"].split("#")[1], {
                                            "Role": newMemberRole
                                        }, function() {
                                            $("#memberViewButtonUpdate" + member["PK"].split("#")[1]).css("background-color", "green");
                                        })
                                    }
                                }),
                                memberRemoveToJquery(member)
                            ])
                        ])
                    ])
                }
            })
        })
    })
}

function getServerInfo(serverID, callback) {
    $.ajax({
        url: baseURL + "/server/get-server",
        type: "GET",
        data: {
            "projectID": projectID,
            "serverID": serverID
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
            if(XMLHttpRequest["responseJSON"] == "Oops, sorry! We can't find any server.") {
                $("#project-view-servers-error").html(XMLHttpRequest["responseJSON"]);
            }
        }
    });
}

function fillServers() {
    if(projectServers.length) {
        projectServers.forEach((server, idx, arr) => {
            getServerInfo(server["SK"], function(response) {
                let serverUnable = true;
                let prepare = "(preparing)";
                if(response["InstanceStatus"]["InstanceStatus"]["Status"] == "ok" && response["InstanceStatus"]["SystemStatus"]["Status"] == "ok")
                    prepare = "";
                if(response["InstanceInfo"]["State"]["Name"] == "running" && response["InstanceStatus"]["InstanceStatus"]["Status"] == "ok" && response["InstanceStatus"]["SystemStatus"]["Status"] == "ok")
                    serverUnable = false
                $("#project-view-servers").append([
                    $("<div>", {class: "server-view-holder"}).append([
                        $("<h2>", {class: "server-title", text: server["SK"]}),
                        $("<p>", {class: "server-init"}).append([
                            "Created: ",
                            $("<span>", {text: server["LaunchTime"]})
                        ]),
                        $("<p>", {class: "server-type"}).append([
                            "Type: ",
                            $("<span>", {text: server["InstanceType"]})
                        ]),
                        $("<p>", {class: "server-status"}).append([
                            "Status: ",
                            $("<span>", {text: response["InstanceInfo"]["State"]["Name"] + prepare})
                        ]),
                        $("<button>", {class: "server-buttons btn", text: "Connect", disabled: serverUnable}).click(function() {
                            // redirectTo("/playground");
                            let targetIP = response["InstanceInfo"]["PublicIpAddress"];
                            // let targetIP = "localhost";
                            localStorage.setItem("targetPlayground", targetIP);
                            let targetURL = "http://" + targetIP + ":1503/playground?";
                            targetURL += "username=" + localStorage.getItem("username") + "&";
                            targetURL += "userAttributes=" + localStorage.getItem("userAttributes") + "&";
                            targetURL += "idToken=" + localStorage.getItem("idToken") + "&";
                            targetURL += "currentProjectID=" + localStorage.getItem("currentProjectID").replace(/\#/g, "%23");

                            redirectToDomain(targetURL);
                        })
                    ])
                ])
            })
        })
    }
    else {
        $("#project-view-servers-error").html("This project has 0 server running");
    }
}

function fillData() {
    fillProjectSummary();
    fillProject();
    fillIssues();
    fillMembers();
    fillServers();
}

function getParams(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var value = url.searchParams.get(param);
    return value;
}

function setInitView() {
    let view = getParams("view");
    if(view) {
        localStorage.setItem("targetProjectView", view);
    }
    let targetProjectView = localStorage.getItem("targetProjectView");
    if(targetProjectView == "Issues") {
        $("#project-menu-project").removeClass("active").addClass("inactive");
        $("#project-menu-issues").removeClass("inactive").addClass("active");

        $("#project-view-project").css("display", "none");
        $("#project-view-issues").css("display", "block");
    }
    else if(targetProjectView == "Members") {
        $("#project-menu-project").removeClass("active").addClass("inactive");
        $("#project-menu-members").removeClass("inactive").addClass("active");

        $("#project-view-project").css("display", "none");
        $("#project-view-members").css("display", "block");
    }
    localStorage.removeItem("targetProjectView");
}

function projectJobs() {
    getUserInfo();
    getProject(function() {
        fillData();
    });
    setInitView();
}

function updateProjectInfo(description, callback) {
    projectInfo["Description"] = description;
    $.ajax({
        url: baseURL + "/project/update-project",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "updateAttributes": {
                "Description": description
            }
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback()
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function updateOwnWorkingInfo(newChange, callback) {
    $.ajax({
        url: baseURL + "/project/update-working-project",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "username": username,
            "updateAttributes": newChange
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback()
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function updateWorkingInfo(targetUsername, newChange, callback) {
    $.ajax({
        url: baseURL + "/project/update-working-project",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "username": targetUsername,
            "updateAttributes": newChange
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback()
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function updateIssueStatus(issueID, newIssueStatus, callback) {
    $.ajax({
        url: baseURL + "/issue/update-issue",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "issueID": issueID,
            "updateAttributes": newIssueStatus
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function assignIssue(issueID, username, callback) {
    $.ajax({
        url: baseURL + "/issue/assign-issue",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "issueID": issueID,
            "username": username
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function removeMember(targetUsername, callback) {
    $.ajax({
        url: baseURL + "/project/delete-working-project",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "username": targetUsername
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function setRecommendListLoading() {
    $(".member-action-members-recommend").css("display", "inline-block");
    $(".member-action-members-recommend").html(
        $("<ul>", {class: "member-action-members-recommend-list"}).html(
            $("<li>")
        )
    );
}

function setRecommendList(itemList) {
    $(".member-action-members-recommend").css("display", "inline-block");
    $(".member-action-members-recommend").html(
        $("<ul>", {class: "member-action-members-recommend-list"})
    );
    itemList.forEach(item => {
        $(".member-action-members-recommend-list").append(
            $("<li>").html(item["PK"]).click(function() {
                let theChosenOne = $(this).html();
                $(".member-action-members-recommend-list").html("");
                $(".member-action-members-recommend-list").css("display", "none");
                $("#newProjectMember").val("");
        
                if(!chosenMemberArray.includes(theChosenOne)) {
                    newMembersArray.push(theChosenOne);
                    $(".member-action-usernames").append(
                        $("<div>", {class: "member-action-username", id: "member-action-username-" + theChosenOne.split("#")[1]}).html([
                            theChosenOne,
                            $("<span>", {text: "×"}).click(function() {
                                $(this).parent().remove();
                                const idx = newMembersArray.indexOf(theChosenOne);
                                if (idx > -1) {
                                    newMembersArray.splice(idx, 1);
                                }
                            })
                        ])
                    )
                }
            })
        )
    })
}

function getRecommendUserList(prefix) {
    $.ajax({
        url: baseURL + "/user/search-user-list",
        type: "GET",
        data: {
            "usernamePrefix": prefix
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        beforeSend: function(xhr) {
            setRecommendListLoading();
        },
        success: function(response){
            // getCorrectedList(response["Items"], setRecommendList);
            setRecommendList(response["Items"]);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function addWorkingProject(username, role, callback) {
    $.ajax({
        url: baseURL + "/project/add-working-project",
        type: "POST",
        data: JSON.stringify({
            "username": username,
            "projectID": projectID,
            "role": role
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function deleteProject(callback) {
    
    // console.log(projectServers);
    // projectServers.forEach(server => {
    //     console.log(projectID);
    //     console.log(server["SK"]);
    //     console.log({
    //         "projectID": projectID,
    //         "serverID": server["SK"]
    //     });
    // })
    

    if(projectServers.length) {
        projectServers.forEach((server, idx, arr) => {
            $.ajax({
                url: baseURL + "/server/delete-server",
                type: "POST",
                data: JSON.stringify({
                    "projectID": projectID,
                    "serverID": server["SK"]
                }),
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": localStorage.getItem("idToken")
                },
                success: function(response){
                    if(idx == arr.length - 1) {
                        $.ajax({
                            url: baseURL + "/project/delete-project",
                            type: "POST",
                            data: JSON.stringify({
                                "projectID": projectID
                            }),
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": localStorage.getItem("idToken")
                            },
                            success: function(response){
                                callback(response)
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log(XMLHttpRequest);
                                console.log(textStatus);
                                console.log(errorThrown);
                            }
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        })
    }
    else {
        $.ajax({
            url: baseURL + "/project/delete-project",
            type: "POST",
            data: JSON.stringify({
                "projectID": projectID
            }),
            headers: {
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem("idToken")
            },
            success: function(response){
                callback(response)
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }


}

function deleteIssue(issueID, callback) {
    $.ajax({
        url: baseURL + "/issue/delete-issue",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "issueID": issueID
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function reloadServers() {
    $.ajax({
        url: baseURL + "/project/get-servers",
        type: "GET",
        data: {
            "projectID": projectID
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            projectServers = response["Items"];
            $("#project-view-servers").html([
                $("<div>", {class: "project-view-servers-buttons"}).html(
                    $("<button>", {class: "btn project-view-servers-button", text: "Reload"}).click(function() {
                        reloadServers();
                    })
                ),
                $("<div>", {class: "clearfix"}),
                $("<p>", {id: "project-view-servers-error"})
            ]);
            fillServers();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

$(document).ready(function() {
    projectJobs();

    // Menu
    $("#project-menu-project").click(function() {
        if($("#project-menu-project").hasClass("inactive")) {
            $("#project-menu-project").removeClass("inactive").addClass("active");
            $("#project-menu-issues").removeClass("active").addClass("inactive");
            $("#project-menu-members").removeClass("active").addClass("inactive");
            $("#project-menu-servers").removeClass("active").addClass("inactive");
            $("#project-menu-delete").removeClass("active").addClass("inactive");

            $("#project-view-project").css("display", "block");
            $("#project-view-issues").css("display", "none");
            $("#project-view-members").css("display", "none");
            $("#project-view-servers").css("display", "none");
            $("#project-view-delete").css("display", "none");
            
            $("#projectSaveStatus").html("");
        }
    });
    $("#project-menu-issues").click(function() {
        if($("#project-menu-issues").hasClass("inactive")) {
            $("#project-menu-project").removeClass("active").addClass("inactive");
            $("#project-menu-issues").removeClass("inactive").addClass("active");
            $("#project-menu-members").removeClass("active").addClass("inactive");
            $("#project-menu-servers").removeClass("active").addClass("inactive");
            $("#project-menu-delete").removeClass("active").addClass("inactive");

            $("#project-view-project").css("display", "none");
            $("#project-view-issues").css("display", "block");
            $("#project-view-members").css("display", "none");
            $("#project-view-servers").css("display", "none");
            $("#project-view-delete").css("display", "none");
        }
    });
    $("#project-menu-members").click(function() {
        if($("#project-menu-members").hasClass("inactive")) {
            $("#project-menu-project").removeClass("active").addClass("inactive");
            $("#project-menu-issues").removeClass("active").addClass("inactive");
            $("#project-menu-members").removeClass("inactive").addClass("active");
            $("#project-menu-servers").removeClass("active").addClass("inactive");
            $("#project-menu-delete").removeClass("active").addClass("inactive");

            $("#project-view-project").css("display", "none");
            $("#project-view-issues").css("display", "none");
            $("#project-view-members").css("display", "block");
            $("#project-view-servers").css("display", "none");
            $("#project-view-delete").css("display", "none");
        }
    });
    $("#project-menu-servers").click(function() {
        if($("#project-menu-servers").hasClass("inactive")) {
            $("#project-menu-project").removeClass("active").addClass("inactive");
            $("#project-menu-issues").removeClass("active").addClass("inactive");
            $("#project-menu-members").removeClass("active").addClass("inactive");
            $("#project-menu-servers").removeClass("inactive").addClass("active");
            $("#project-menu-delete").removeClass("active").addClass("inactive");

            $("#project-view-project").css("display", "none");
            $("#project-view-issues").css("display", "none");
            $("#project-view-members").css("display", "none");
            $("#project-view-servers").css("display", "block");
            $("#project-view-delete").css("display", "none");
        }
    });
    $("#project-menu-delete").click(function() {
        if($("#project-menu-delete").hasClass("inactive")) {
            $("#project-menu-project").removeClass("active").addClass("inactive");
            $("#project-menu-issues").removeClass("active").addClass("inactive");
            $("#project-menu-members").removeClass("active").addClass("inactive");
            $("#project-menu-servers").removeClass("active").addClass("inactive");
            $("#project-menu-delete").removeClass("inactive").addClass("active");

            $("#project-view-project").css("display", "none");
            $("#project-view-issues").css("display", "none");
            $("#project-view-members").css("display", "none");
            $("#project-view-servers").css("display", "none");
            $("#project-view-delete").css("display", "block");
        }
    });

    // Project view
    $("#projectOK").click(function() {
        $("#projectSaveStatus").html("");
        let newProjectDescription = $("#projectDescription").val();
        if(newProjectDescription != projectInfo["Description"]) {
            updateProjectInfo(newProjectDescription, function() {
                let newRole = $("#projectRole").val();
                let newStar = $("#projectStar").is(":checked");
                let newWorkingChange = {};
                if(newRole != ownWorkingProjectItem["Role"]) {
                    newWorkingChange["Role"] = newRole;
                    ownWorkingProjectItem["Role"] = newRole;
                }
                if(newStar != ownWorkingProjectItem["Starred"]) {
                    newWorkingChange["Starred"] = newStar;
                    ownWorkingProjectItem["Starred"] = newStar;
                }
                if(Object.keys(newWorkingChange).length) {
                    updateOwnWorkingInfo(newWorkingChange, function() {
                        $("#projectSaveStatus").html("Saved");
                        $("#projectSummaryRole").html(newRole);
                        $("#projectSummaryStar").html(newStar.toString());
                    });
                }
                else {
                    $("#projectSaveStatus").html("Saved");
                }
            });
        }
        else {
            let newRole = $("#projectRole").val();
            let newStar = $("#projectStar").is(":checked");
            let newWorkingChange = {};
            if(newRole != ownWorkingProjectItem["Role"]) {
                newWorkingChange["Role"] = newRole;
                ownWorkingProjectItem["Role"] = newRole;
            }
            if(newStar != ownWorkingProjectItem["Starred"]) {
                newWorkingChange["Starred"] = newStar;
                ownWorkingProjectItem["Starred"] = newStar;
            }
            if(Object.keys(newWorkingChange).length) {
                updateOwnWorkingInfo(newWorkingChange, function() {
                    $("#projectSaveStatus").html("Saved");
                    $("#projectSummaryRole").html(newRole);
                    $("#projectSummaryStar").html(newStar.toString());
                });
            }
            else {
                $("#projectSaveStatus").html("Nothing changed");
            }
        }
    })

    // Issue view
    $(".project-view-issues-button").click(function() {
        redirectTo("/new-issue")
    })

    // Member view
    let memberTimeout = null;
    // Member list recommend
    $("#newProjectMember").on("paste keyup", function() {
        if(memberTimeout != null) {
            clearTimeout(memberTimeout);
        }
        memberTimeout = setTimeout(function() {
            if($("#newProjectMember").val() != "") {
                getRecommendUserList($("#newProjectMember").val());
            } else {
                $(".member-action-members-recommend").css("display", "none");
            }
            memberTimeout = null;
        }, 1000);
    });
    $("#memberActionButtonAdd").click(function() {
        let newMembersRole = $("#memberActionRole").val();
        newMembersArray.forEach((chosenMember, idx, arr) => {
            addWorkingProject(chosenMember.split("#")[1], newMembersRole, function(response) {
                $("#member-action-username-" + chosenMember.split("#")[1]).remove();
                let newWorkingItem = response["Item"];
                addMemberList(newWorkingItem);
                if(idx == arr.length - 1) {
                    newMembersArray = [];
                }
            })
        })
    })

    // Server view
    $(".project-view-servers-button").click(function() {
        reloadServers();
    })

    // Delete view
    $("#deleteButton").click(function() {
        // Loader
        $("#deleteLoader").css("display", "inline-block");
        deleteProject(function(response) {
            console.log(response);
            redirectTo("/dashboard");
        })
    });
    $("#deleteInput").on("paste keyup", function() {
        let deleteInput = $("#deleteInput").val();
        if(deleteInput != "Delete") {
            $("#deleteButton").attr("disabled", "disabled");
        }
        else {
            $("#deleteButton").removeAttr("disabled");
        }
    })

    // Back button
    $(".back-button button").click(function() {
        localStorage.removeItem("targetIssue");
        redirectTo("/dashboard");
    })

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
