// var accessToken = null;
// var refreshToken = null;
// var idToken = null;
var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";
var username = localStorage.getItem("username");
var userID = "User#" + username;
var workingProjects = [];
var chosenProjectID = [];
var existingGroupName = [];

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

function setUserInfo(userInfos) {
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = userInfos["UserAttributes"];
        if(userAttributes.hasOwnProperty("picture")) {
            $(".avatar").attr("src", userAttributes["picture"]);
        }
    }
}

function getUserInfo() {
    let userInfos = {
        "Username": localStorage.getItem("username"),
        "UserAttributes": JSON.parse(localStorage.getItem("userAttributes"))
    }
    setUserInfo(userInfos);
}

function getWorkingProjects() {
    $.ajax({
        url: baseURL + "/project/get-working-projects",
        type: "GET",
        data: {
            "username": username
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            response["Items"].forEach(item => {
                workingProjects.push(item);
            })
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function getExistingGroup() {
    $.ajax({
        url: baseURL + "/group/get-groups",
        type: "GET",
        data: {
            "username": username
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            response["Items"].forEach(group => {
                existingGroupName.push(group["PK"].split("#")[2]);
            })
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function newGroupJobs() {
    getUserInfo();
    getWorkingProjects();
    getExistingGroup();
}

function getRecommendList(prefix) {
    let initList = workingProjects.slice(0);
    let resultList = [];

    initList.forEach((project, idx, arr) => {
        if(project["SK"].includes(prefix)) {
            resultList.push(project);
        }
        if(idx >= arr.length - 1) {
            setRecommendList(resultList);
        }
    })
}

function setRecommendList(itemList) {
    $(".new-group-form-projects-recommend").css("display", "inline-block");
    $(".new-group-form-projects-recommend").html(
        $("<ul>", {class: "new-group-form-projects-recommend-list"})
    );
    itemList.forEach(item => {
        $(".new-group-form-projects-recommend-list").append(
            $("<li>").html(item["SK"]).click(function() {
                let theChosenOne = $(this).html();
                $(".new-group-form-projects-recommend-list").html("");
                $(".new-group-form-projects-recommend-list").css("display", "none");
                $("#newGroupProject").val("");
                
                if(!chosenProjectID.includes(theChosenOne)) {
                    chosenProjectID.push(theChosenOne);
                    $(".new-group-projects-names").append(
                        $("<div>", {class: "new-group-projects-name", id: "new-group-projects-name-" + theChosenOne}).html([
                            theChosenOne,
                            $("<span>", {text: "×"}).click(function() {
                                $(this).parent().remove();
                                const idx = chosenProjectID.indexOf(theChosenOne);
                                if (idx > -1) {
                                    chosenProjectID.splice(idx, 1);
                                }
                            })
                        ])
                    )
                }
            })
        )
    })
}

function addGroup(name, des, publicy, projectList, callback) {
    $.ajax({
        url: baseURL + "/group/add-group",
        type: "POST",
        data: JSON.stringify({
            "username": username,
            "groupName": name,
            "isPublic": publicy,
            "description": des,
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            if(projectList.length > 0) {
                projectList.forEach((projectID, idx, arr) => {
                    $.ajax({
                        url: baseURL + "/group/add-project",
                        type: "POST",
                        data: JSON.stringify({
                            "projectID": projectID,
                            "groupID": response["Item"]["PK"]
                        }),
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": localStorage.getItem("idToken")
                        },
                        success: function(response){
                            if(idx == arr.length - 1)
                                callback(response);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                })
            }
            else {
                callback(response)
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


$(document).ready(function() {
    newGroupJobs();

    let currentStep = 1;

    // Recommend project list
    $("#newGroupProject").click(function() {
        getRecommendList($("#newGroupProject").val());
    })
    $("#newGroupProject").on("paste keyup", function() {
        getRecommendList($("#newGroupProject").val());
    })

    // Stop display recommend
    $(document).click(function(event) { 
        var $target = $(event.target);
        if(!$target.closest('#newGroupProject').length && $('.new-group-form-projects-recommend').css("display") != "none") {
            $('.new-group-form-projects-recommend').css("display", "none");
        }        
      });

    // Stop error display
    $("#newGroupName").click(function() {
        $("#newGroupName").css("border-color", "rgb(175, 175 ,175)");
        $("#groupNameError").html("");
    });
    $("#newGroupProject").click(function() {
        $("#newGroupProject").css("border-color", "rgb(175, 175 ,175)");
        $("#groupProjectError").html("");
    });

    // Form button
    $(".btn.next").click(function() {
        if(currentStep == 1 && $("#newGroupName").val().trim() == ""){
            $("#newGroupName").css("border-color", "red");
            $("#groupNameError").html("This field cannot be empty");
        }
        else if(currentStep == 1 && existingGroupName.includes($("#newGroupName").val().trim())){
            $("#newGroupName").css("border-color", "red");
            $("#groupNameError").html("Group name is already exists.");
        }
        else if(currentStep == 1 && chosenProjectID.length == 0) {
            $("#newGroupProject").css("border-color", "red");
            $("#groupProjectError").html("Group require atleast 1 project inside.");
        }
        else {
            if($("#newGroupName").val() != "")
                $("#new-group-form-review-name").html($("#newGroupName").val());
            if($("#newGroupDescription").val() != "")
                $("#new-group-form-review-description").html($("#newGroupDescription").val());
            $("#new-group-form-review-publicy").html($("#newGroupPublicy").val());
            if(chosenProjectID.length != 0)
                chosenProjectID.forEach(projectID => {
                    $("#new-group-form-review-project").append(projectID);
                })

            $("#new-group-steps-item-" + currentStep).removeClass("active").addClass("inactive");
            $("#new-group-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
            $(".new-group-form-" + currentStep).css("display", "none");
            $(".new-group-form-" + (currentStep + 1)).css("display", "block");
            currentStep++;
        }
    });
    $(".btn.cancel").click(function() {
        redirectTo("/group-projects");
    });
    $(".btn.previous").click(function() {
        $("#new-group-steps-item-" + currentStep).removeClass("active").addClass("inactive");
        $("#new-group-steps-item-" + (currentStep - 1)).removeClass("inactive").addClass("active");
        $(".new-group-form-" + currentStep).css("display", "none");
        $(".new-group-form-" + (currentStep - 1)).css("display", "block");
        currentStep--;
    });
    $(".btn.finish").click(function() {

        // Loader
        $(".new-group-form-entry div.loader").css("display", "inline-block");

        // Get form info
        let groupName = $("#newGroupName").val().trim();
        let groupDescription = $("#newGroupDescription").val();
        let groupPublicy = $("#newGroupPublicy").val();

        addGroup(groupName, groupDescription, groupPublicy, chosenProjectID, function(response) {
            redirectTo("/group-projects");
        })

    });

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
