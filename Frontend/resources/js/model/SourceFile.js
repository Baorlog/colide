class SourceFile {
    constructor(filePath) {
        this.filePath = filePath;
        this.fileType = null;
        this.editor = null;
        
        var filePathComponents = filePath.split('.');
        var fileType = SourceFile.getFullFileType(filePathComponents[filePathComponents.length - 1]);

        this.fileType = fileType;
    }

    // Static methods

    static getFullFileType(extension) {
        switch(extension) {
            case "js": case "json":
                return "javascript";
            case "py":
                return "python";
            case "html": case "htm":
                return "html";
            case "css":
                return "css";
            default:
                return extension;
        }
    }

    static deleteOldCMEditor(cmInstance) {
        if(cmInstance != null && cmInstance.editor != null) {
            cmInstance.setValue("");
            cmInstance.editor.toTextArea();
            cmInstance = null;
        }
    }

    // Instance methods

    setValue(content) {
        // this.editor.setValue(content);
        this.editor.replaceRange(content, {"line": 0, "ch": 0, "sticky": null}, {"line": 0, "ch": 0, "sticky": null}, "@ignore");
    }

    getValue() {
        return this.editor.getValue();
    }

    initiateCMEditor(editorID, theme, filePath) {
        /**
         * Configuration CodeMirror
         */

        let cmConfiguration = {
            mode: this.fileType,
            theme: theme,
            lineNumbers: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            indentUnit: 4,
            indentWithTabs: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
            extraKeys: {
                "Ctrl-/": function(cm) {
                    cm.execCommand('toggleComment');
                },
                "Ctrl-Space": "autocomplete"
            },
            lineWrapping: true,
            foldGutter: true,
            // autoCloseTags: true,
            // matchTags: false,
            // gutters: ["CodeMirror-linenumbers", "breakpoints"]
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        };

        if(this.fileType == "html") {
            cmConfiguration.mode = "htmlmixed";
        }

        // Create CodeMirror Editor
        $(".playground-content").html(
            $("<form>").html(
                $("<textarea>", {id: "codeEditor", name: "codeEditor", readonly: "readonly"})
            )
        );
        this.editor = CodeMirror.fromTextArea(document.getElementById(editorID), cmConfiguration);

        this.editor.setSize("100%","67.3vh");


        /**
         * Auto complete
         */

        function isValidForAutoComplete(keyCode) {
            var restrictKeys = [
                8,      //Backspace
                13,     //Return
                17,     //Ctrl
                18,     //Alt
                32,     //Space
                37,     //Left arrow
                38,     //Up arrow
                39,     //Right arrow
                40,     //Down arrow
                // 46,     //Delete
                58,     //Colon
                59,     //Semi-colon
                91,     //[
                93,     //]
                123,    //{
                125,    //}
                127,    //Delete
                219,    //[{
                221     //]}
            ];
            if(restrictKeys.includes(keyCode)) {
                return false;
            }
            else {
                return true;
            }
        }

        /**
         * Event listeners
         */

        this.editor.on("keypress", function (cm, event) {
            if (!cm.state.completionActive && /*Enables keyboard navigation in autocomplete list*/  
                isValidForAutoComplete(event.keyCode)) {         
                CodeMirror.commands.autocomplete(cm, null, {completeSingle: false});
            }
        });

        this.editor.on("change", function(cm, event) {
            if(event.origin != "@ignore") {
                event["currentFilePath"] = filePath;
                socket.emit('CMEditorChange', event);
            }
        })

        this.editor.on("cursorActivity", function(cm) {
            var cursorPos = cm.getCursor();
            socket.emit('CMCursor', {
                "cursorPos": cursorPos,
                "currentFilePath": filePath
            });
        })

        this.editor.on('blur', function () { 
            $(".CodeMirror-cursors").css('visibility', 'visible'); 
        });
    }

    getFileType() {
        return this.fileType;
    }
}