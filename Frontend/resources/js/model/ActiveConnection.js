class ActiveConnection{
    constructor(username, color) {
        if(username)
            this.username = username;
        else
            this.username = "Guest"
        this.color = color;
        this.openFile = null;

        this.displayActiveConnection();
    }

    displayActiveConnection() {
        $(".active-connections-list").append(
            $("<li>", {id: "active-connection-" + this.username}).html([
                $("<div>", {class: "active-connection-name", text: this.username}).css("color", this.color),
                $("<div>", {class: "active-connection-file", text: "<none>"})
            ])
        )
    }

    removeActiveConnection() {
        $("#active-connection-" + this.username).remove();
    }

    changeOpenFile(file) {
        this.openFile = file;
        $("#active-connection-" + this.username).children().last().html(file);
    }
}