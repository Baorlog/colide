class Tab{
    constructor(fileInfo){
        this.filePath = fileInfo.path;
        this.fileName = fileInfo.name;

        if(!openTabs.hasOwnProperty(this.filePath)) {
            openTabs[this.filePath] = this;

            this.createNewTab(this.filePath);
        }
        else {
            $(".playground-tabs-item.active").removeClass("active").addClass("inactive");
            $("#playground-tabs-item-" + this.filePath.replace(/\//g, "-").replace(/\./g, "-").replace(/\#/g, "-")).removeClass("inactive").addClass("active");
        }
    }

    createNewTab(filePath) {
        $(".playground-tabs-item.active").removeClass("active").addClass("inactive");
        $(".playground-tabs").append(
            $("<div>", {class: "playground-tabs-item active", id: "playground-tabs-item-" + filePath.replace(/\//g, "-").replace(/\./g, "-").replace(/\#/g, "-")}).html([
                $("<div>", {class: "playground-tabs-item-name", text: this.fileName}).click(function() {
                    socket.emit("GetFile", filePath);
                    $(".playground-tabs-item.active").removeClass("active").addClass("inactive");
                    $(this).parent().removeClass("inactive").addClass("active");
                }),
                $("<div>", {class: "playground-tabs-item-close", text: "×"}).click(function() {
                    $(this).parent().remove();
                    delete openTabs[filePath];
                    if(currentOpenFile == filePath) {
                        $(".playground-content").html("");
                        cmInstance = null;
                    }
                })
            ])
        )
    }
}