class ModalBox {
    constructor(DOMElementReference, modalType, modalData) {
        this.DOMElementReference = DOMElementReference;
        this.modalType = modalType;
        this.modalData = modalData;

        this.createModalBox(this.createModalJson(this.modalType, this.modalData));
    }

    /**
     * JSON format
     * {
     *      title: "Title name",
     *      entries: [
     *          {
     *              label: "Label Name",
     *              inputType: "text" || "image" || "file",
     *              prefix: "text...",
     *              placeholder: "here",
     *              initValue: "Some value",
     *              suffix: "text...",
     *              guide: "text..."
     *          },...
     *      ],
     *      func: function() {}
     * }
     */

    createModalBox(dataJson) {
        this.DOMElementReference.html(
            $("<div>", { class: "modal-content" }).append([
                $("<div>", { class: "modal-header" }).append(
                    $("<h3>", { text: dataJson.title }), 
                    $("<span>", { class: "modal-close", text: "×" }).click(() => {
                        this.DOMElementReference.css("display", "none");
                    })
                ),
                $("<div>", { class: "modal-body" })
            ])
        )

        dataJson.entries.forEach((entry, index, array) => {
            // Select editor theme
            if(entry.inputType == "select" && dataJson.type == "Theme") {
                $(".modal-body").append(
                    $("<div>", {class: "modal-body-row"}).append([
                        $("<div>", {class: "modal-body-row-label col span-1-of-3", id: "modalBodyRowLabel" + index, text: entry.label}),
                        $("<div>", {class: "modal-body-row-input col span-2-of-3", id: "modalBodyRowInput" + index}).append([
                            $("<span>", {class: "modal-body-row-input-prefix", id: "modalBodyRowInputPrefix" + index, text: entry.prefix}),
                            $("<select>", {class: "modal-body-row-input-input",id: "modalBodyRowInputInput" + index}).html([
                                $("<option>", {value: "3024-day", text: "3024-day"}),
                                $("<option>", {value: "3024-night", text: "3024-night"}),
                                $("<option>", {value: "abcdef", text: "abcdef"}),
                                $("<option>", {value: "ambiance-mobile", text: "ambiance-mobile"}),
                                $("<option>", {value: "ambiance", text: "ambiance"}),
                                $("<option>", {value: "ayu-dark", text: "ayu-dark"}),
                                $("<option>", {value: "ayu-mirage", text: "ayu-mirage"}),
                                $("<option>", {value: "base16-dark", text: "base16-dark"}),
                                $("<option>", {value: "base16-light", text: "base16-light"}),
                                $("<option>", {value: "bespin", text: "bespin"}),
                                $("<option>", {value: "blackboard", text: "blackboard"}),
                                $("<option>", {value: "cobalt", text: "cobalt"}),
                                $("<option>", {value: "colorforth", text: "colorforth"}),
                                $("<option>", {value: "darcula", text: "darcula"}),
                                $("<option>", {value: "dracula", text: "dracula"}),
                                $("<option>", {value: "duotone-dark", text: "duotone-dark"}),
                                $("<option>", {value: "duotone-light", text: "duotone-light"}),
                                $("<option>", {value: "eclipse", text: "eclipse"}),
                                $("<option>", {value: "elegant", text: "elegant"}),
                                $("<option>", {value: "erlang-dark", text: "erlang-dark"}),
                                $("<option>", {value: "gruvbox-dark", text: "gruvbox-dark"}),
                                $("<option>", {value: "hopscotch", text: "hopscotch"}),
                                $("<option>", {value: "icecoder", text: "icecoder"}),
                                $("<option>", {value: "idea", text: "idea"}),
                                $("<option>", {value: "isotope", text: "isotope"}),
                                $("<option>", {value: "lesser-dark", text: "lesser-dark"}),
                                $("<option>", {value: "liquibyte", text: "liquibyte"}),
                                $("<option>", {value: "lucario", text: "lucario"}),
                                $("<option>", {value: "material-darker", text: "material-darker"}),
                                $("<option>", {value: "material-ocean", text: "material-ocean"}),
                                $("<option>", {value: "material-palenight", text: "material-palenight"}),
                                $("<option>", {value: "material", text: "material"}),
                                $("<option>", {value: "mbo", text: "mbo"}),
                                $("<option>", {value: "mdn-like", text: "mdn-like"}),
                                $("<option>", {value: "midnight", text: "midnight"}),
                                $("<option>", {value: "monokai", text: "monokai"}),
                                $("<option>", {value: "moxer", text: "moxer"}),
                                $("<option>", {value: "neat", text: "neat"}),
                                $("<option>", {value: "neo", text: "neo"}),
                                $("<option>", {value: "night", text: "night"}),
                                $("<option>", {value: "nord", text: "nord"}),
                                $("<option>", {value: "oceanic-next", text: "oceanic-next"}),
                                $("<option>", {value: "panda-syntax", text: "panda-syntax"}),
                                $("<option>", {value: "paraiso-dark", text: "paraiso-dark"}),
                                $("<option>", {value: "paraiso-light", text: "paraiso-light"}),
                                $("<option>", {value: "pastel-on-dark", text: "pastel-on-dark"}),
                                $("<option>", {value: "railscasts", text: "railscasts"}),
                                $("<option>", {value: "rubyblue", text: "rubyblue"}),
                                $("<option>", {value: "seti", text: "seti"}),
                                $("<option>", {value: "shadowfox", text: "shadowfox"}),
                                $("<option>", {value: "solarized", text: "solarized"}),
                                $("<option>", {value: "ssms", text: "ssms"}),
                                $("<option>", {value: "the-matrix", text: "the-matrix"}),
                                $("<option>", {value: "tomorrow-night-bright", text: "tomorrow-night-bright"}),
                                $("<option>", {value: "tomorrow-night-eighties", text: "tomorrow-night-eighties"}),
                                $("<option>", {value: "ttcn", text: "ttcn"}),
                                $("<option>", {value: "twilight", text: "twilight"}),
                                $("<option>", {value: "vibrant-ink", text: "vibrant-ink"}),
                                $("<option>", {value: "xq-dark", text: "xq-dark"}),
                                $("<option>", {value: "xq-light", text: "xq-light"}),
                                $("<option>", {value: "yeti", text: "yeti"}),
                                $("<option>", {value: "yonce", text: "yonce"}),
                                $("<option>", {value: "zenburn", text: "zenburn"})
                            ]).val(localStorage.getItem("editorTheme")),
                            $("<span>", {class: "modal-body-row-input-suffix", id: "modalBodyRowInputSuffix" + index, text: entry.suffix})
                        ])
                    ]),
                );
            }
            // Other toolbar menu modal
            else {
                $(".modal-body").append(
                    $("<div>", {class: "modal-body-row"}).append([
                        $("<div>", {class: "modal-body-row-label col span-1-of-3", id: "modalBodyRowLabel" + index, text: entry.label}),
                        $("<div>", {class: "modal-body-row-input col span-2-of-3", id: "modalBodyRowInput" + index}).append([
                            $("<span>", {class: "modal-body-row-input-prefix", id: "modalBodyRowInputPrefix" + index, text: entry.prefix}),
                            $("<input>", {class: "modal-body-row-input-input", type: entry.inputType, name: "modalBodyRowInputInput" + index, id: "modalBodyRowInputInput" + index, placeholder: entry.placeholder}),
                            $("<span>", {class: "modal-body-row-input-suffix", id: "modalBodyRowInputSuffix" + index, text: entry.suffix})
                        ])
                    ]),
                );
            }

            if(entry.hasOwnProperty("guide")) {
                $(".modal-body").append(
                    $("<div>", {class: "modal-body-row"}).append([
                        $("<div>", {class: "modal-body-row-label col span-1-of-3", text: ""}),
                        $("<div>", {class: "modal-body-row-input col span-2-of-3"}).append(
                            $("<div>", {class: "modal-body-row-input-guide", id: "modalBodyRowInputGuide" + index, text: entry.guide})
                        )
                    ])
                );
            };

            if(entry.hasOwnProperty("initValue")) {
                $("#modalBodyRowInputInput" + index).val(entry.initValue);
            }

            if(index == array.length - 1) {
                $(".modal-body").append(
                    $("<div>", {class: "modal-body-buttons"}).append(
                        $("<button>", {class: "btn", id: "modalBodyButtonOK", text: "OK"}).click(() => {
                            // Get inputs
                            let inputs = [];
                            for(var i = 0; i < array.length; i++) {
                                inputs.push($("#modalBodyRowInputInput" + i).val())
                            }
                            console.log(inputs);
                    
                            // Handle input
                            dataJson.func(inputs);
                        }),
                        $("<button>", {class: "btn", id: "modalBodyButtonCancel", text: "Cancel"}).click(() => {
                            this.DOMElementReference.css("display", "none");
                        })
                    )
                );
            }
        });
    }

    createModalJson(type, data) {
        let codebaseName = localStorage.getItem("currentProjectID");
        switch(type) {
            case "New File":
                return {
                    type: type,
                    title: "New File",
                    entries: [
                        {
                            label: "File name",
                            inputType: "text",
                            prefix: codebaseName + "/ ",
                            placeholder: "",
                            suffix: "",
                        },
                    ],
                    func: (inputs) => {
                        if(inputs[0] != "") {
                            $("#modalBodyRowInputInput0").val("");
                            let fullFilePath = codebaseName + "/" + inputs[0];
                
                            // Work with server
                            console.log(`Creating file ${fullFilePath}...`)
                            socket.emit('FileChange', {
                                "type": "new-file",
                                "path": Codebase.backSlashToSlash(fullFilePath)
                            });
                            this.DOMElementReference.css("display", "none");
                        } else {
                            alert("File path is empty");
                            $("#modalBodyRowInputInput0").css("border-color", "red");
                        }
                    }
                };
            case "New Folder":
                return {
                    type: type,
                    title: "New Folder",
                    entries: [
                        {
                            label: "Directory name",
                            inputType: "text",
                            prefix: codebaseName + "/ ",
                            placeholder: "",
                            suffix: "",
                        },
                    ],
                    func: (inputs) => {
                        if(inputs[0] != "") {
                            $("#modalBodyRowInputInput0").val("");
                            let fullDirectoryPath = codebaseName + "/" + inputs[0];
                
                            // Work with server
                            console.log(`Creating directory ${fullDirectoryPath}...`)
                            socket.emit('FileChange', {
                                "type": "new-folder",
                                "path": Codebase.backSlashToSlash(fullDirectoryPath)
                            });
                            this.DOMElementReference.css("display", "none");
                        } else {
                            alert("Directory path is empty");
                            $("#modalBodyRowInputInput0").css("border-color", "red");
                        }
                    }
                };
            case "Delete":
                let deletingFiles = data[0].join("\n ");
                return {
                    type: type,
                    title: "Delete object(s) and all its component(s)",
                    entries: [
                        {
                            label: "Deleting object(s)",
                            inputType: "hidden",
                            prefix: deletingFiles,
                            placeholder: "",
                            suffix: "",
                        }
                    ],
                    func: (inputs) => {
                        // Work with server
                        data[1].forEach(fileInfo => {
                            console.log(`Deleting file ${fileInfo.id}...`)
                            socket.emit('FileChange', {
                                "type": "delete",
                                "path": Codebase.backSlashToSlash(fileInfo.id),
                                "fileType": fileInfo.fileType
                            })
                        });
                        this.DOMElementReference.css("display", "none");
                    }
                }
            case "Rename":
                let filePathElements = ModalBox.extractPath(ModalBox.backSlashToSlash(data[0]));
                let fileDirectory = filePathElements[0];
                let fileName = filePathElements[1];
                return {
                    type: type,
                    title: "Rename",
                    entries: [
                        {
                            label: "Renaming object",
                            inputType: "text",
                            prefix: fileDirectory,
                            placeholder: "",
                            initValue: fileName,
                            suffix: "",
                        }
                    ],
                    func: (inputs) => {
                        // Work with server
                        console.log(`Renaming object ${data[0]} to ${inputs[0]}...`)
                        socket.emit('FileChange', {
                            "type": "rename",
                            "newPath": Codebase.backSlashToSlash(fileDirectory + inputs[0]),
                            "oldPath": Codebase.backSlashToSlash(data[0])
                        })
                        this.DOMElementReference.css("display", "none");
                    }
                }
            case "Theme":
                return {
                    type: type,
                    title: "Editor theme",
                    entries: [
                        {
                            label: "Theme",
                            inputType: "select",
                            prefix: "",
                            placeholder: "",
                            initValue: localStorage.getItem("editorTheme"),
                            suffix: "",
                        }
                    ],
                    func: (input) => {
                        let newTheme = input[0];
                        // Change at local
                        if(cmInstance != null) {
                            cmInstance.editor.setOption("theme", newTheme);
                        }
                        localStorage.setItem("editorTheme", newTheme);

                        // Work with server
                        $.ajax({
                            url: baseURL + "/user/update-user-details",
                            type: "POST",
                            data: JSON.stringify({
                                "accessToken": localStorage.getItem("accessToken"),
                                "updateAttributes": {
                                    "custom:editorTheme": newTheme
                                }
                            }),
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": localStorage.getItem("idToken")
                            },
                            success: function(response){
                                console.log("Theme change to " + newTheme + ": " + response["Success"])
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log(XMLHttpRequest);
                                console.log(textStatus);
                                console.log(errorThrown);
                            }
                        });
                        this.DOMElementReference.css("display", "none");
                    }
                }
            default:
                console.log("Unknown Modal Json type");
        }
    }

    static backSlashToSlash(string) {
        return string.replace(/\\/g, "/");
    }
    
    static extractPath(fullPath) {
        let pathElements = ModalBox.backSlashToSlash(fullPath).split("/");
        let fileName = pathElements.pop();
        let dir = pathElements.join("/") + "/";
        return [dir, fileName];
    }
}



// this.DOMElementReference.html(
//     $("<div>", { class: "modal-content" }).append([
//         $("<div>", { class: "modal-header" }).append(
//             $("<h3>", { text: dataJson.title }), 
//             $("<span>", { class: "modal-close", text: "×" }).click(() => {
//                 this.DOMElementReference.css("display", "none");
//             })
//         ),
//         $("<div>", { class: "modal-body"}).append([
//             $("<div>", {class: "modal-body-row"}).append([
//                 $("<div>", {class: "modal-body-row-label col span-1-of-3", text: "File name: "}),
//                 // $("<div>", {class: "modal-body-row-label col-md-4", text: "File name: "}),
//                 // $("<div>", {class: "modal-body-row-label col-md-4", text: "File name: "}),
//                 // $("<div>", {class: "modal-body-row-label col-md-4", text: "File name: "}),
//                 $("<div>", {class: "modal-body-row-input col span-2-of-3"}).append([
//                     $("<span>", {class: "modal-body-row-input-prefix", text: "codebase/ "}),
//                     $("<input>", {class: "modal-body-row-input-input", type: "text", name: "modalBoxNewFileInput", id: "modalBoxNewFileInput"}),
//                     $("<span>", {class: "modal-body-row-input-suffix", text: ".txt"})
//                 ])
//             ]),
//             $("<div>", {class: "modal-body-row"}).append([
//                 $("<div>", {class: "modal-body-row-label col span-1-of-3", text: ""}),
//                 $("<div>", {class: "modal-body-row-input col span-2-of-3"}).append([
//                     $("<div>", {class: "modal-body-row-input-guide", text: "Something about this input"})
//                 ])
//             ]),
//             $("<div>", {class: "modal-body-buttons"}).append(
//                 $("<button>", {class: "btn", id: "modalBoxNewFileOK", text: "OK"}).click(() => {
//                     // Get inputs
//                     let filePath = $("#modalBoxNewFileInput").val();
            
//                     // Handle input
//                     if(filePath != "") {
//                         $("#modalBoxNewFileInput").val("");
//                         let fullFilePath = "codebase/" + filePath;
            
//                         // Work with server
//                         console.log(`Creating file ${fullFilePath}...`)
//                         socket.emit('FileChange', {
//                             "type": "new-file",
//                             "path": Codebase.backSlashToSlash(fullFilePath)
//                         });
//                         this.DOMElementReference.css("display", "none");
//                     } else {
//                         alert("File path is empty");
//                         $("#modalBoxNewFileInput").css("border-color", "red");
//                     }
//                 }),
//                 $("<button>", {class: "btn", id: "modalBoxNewFileCancel", text: "Cancel"}).click(() => {
//                     this.DOMElementReference.css("display", "none");
//                 })
//             )
//         ])
//     ])
// )