class Chat{
    constructor(chatInfo) {
        this.author = chatInfo.author;
        this.content = chatInfo.content;
        this.color = chatInfo.color;

        this.displayChatMessage();
    }

    displayChatMessage() {
        $(".chat-content").append(
            $("<div>", {class: "chat-content-item"}).html([
                $("<div>", {class: "chat-content-item-author", text: this.author}).css("color", this.color),
                $("<div>", {class: "chat-content-item-body", text: this.content})
            ])
        )
    }

    static displayChatNotification(notiMessage) {
        $(".chat-content").append(
            $("<div>", {class: "chat-content-notification", text: notiMessage})
        )
    }
}