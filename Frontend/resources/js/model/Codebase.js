class Codebase {
    constructor(codebaseInformation) {
        this.codebase = codebaseInformation;
        // Codebase.codebaseTraversal(this.codebase);
        // console.log(typeof(Codebase.generateHTMLArray(this.codebase)));
        // let string = Codebase.convertHTMLArrayToString(Codebase.generateHTMLArray(this.codebase),""); 
        // console.log(string);
        // let string = Codebase.getJsTreeJSON(this.codebase);
        // console.log(string);
    }

    static codebaseTraversal(tree) {
        console.log(tree.path);
        if(tree.type == "directory") {
            for(const child in tree.children) {
                // console.log(tree.children.child);
                Codebase.codebaseTraversal(tree.children[child]);
            }
        }
    }

    // static generateHTMLArray(tree) {
    //     if(tree.type == "directory") {
    //         let htmlArray = [tree.name, "<ul>"];
    //         for(const child in tree.children) {
    //             htmlArray.push("<li>");
    //             htmlArray.push(Codebase.generateHTMLArray(tree.children[child]));
    //             htmlArray.push("</li>");
    //         }
    //         htmlArray.push("</ul>");
    //         return htmlArray;
    //     } else {
    //         return tree.name;
    //     }
    // }
    
    // static convertHTMLArrayToString(htmlArray, resultString) {
    //     // console.log(htmlArray);
    //     for(const element in htmlArray) {
    //         if(typeof(htmlArray[element]) == "object") {
    //             resultString += Codebase.convertHTMLArrayToString(htmlArray[element], "");
    //         } else if(typeof(htmlArray[element]) == "string") {
    //             resultString += htmlArray[element];
    //         }
    //     }
    //     return resultString;
    // }

    static getJsTreeJSON(codebase) {
        let resultJSON = {};
        resultJSON.id = Codebase.backSlashToSlash(codebase.path);
        resultJSON.text = codebase.name;
        resultJSON.state = {
            selected: false
        };

        if(codebase.type == "directory") {
            resultJSON.icon = Codebase.getFontAwesomeIcon();
            let treeChildren = [];
            for(const child in codebase.children) {
                treeChildren.push(Codebase.getJsTreeJSON(codebase.children[child]));
            }
            resultJSON.children = treeChildren;
            resultJSON.type = "directory";
        } else if(codebase.type == "file") {
            resultJSON.icon = Codebase.getFontAwesomeIcon(codebase.extension);
            resultJSON.type = "file";
        }
        return resultJSON;
    }

    static getFontAwesomeIcon(extension) {
        let iconString = ""
        if(extension === undefined) {                                                       // Directory
            iconString = "far fa-folder-open";
        } else if(extension == ".html" || extension == ".htm") {                            // HTML
            iconString = "fas fa-code";
        } else if (extension == ".js") {                                                    // Javascript
            iconString = "fab fa-js";
        } else if (extension == ".css") {                                                   // CSS
            iconString = "fab fa-css3";
        } else if(extension == ".jpg" || extension == ".png" || extension == ".jpeg") {     // Image
            iconString = "far fa-file-image";
        } else if(extension == ".txt") {                                                    // Text
            iconString = "far fa-file-alt";
        } else if(extension == ".zip" || extension == ".rar") {                             // Compress file
            iconString = "far fa-file-archive";
        } else if(extension == ".py") {                                                     // Python
            iconString = "fab fa-python";
        } else if(extension == ".md" || extension == ".rst") {                              // Markdown, readme
            iconString = "fab fa-readme";
        } else {                                                                            // Others
            iconString = "far fa-file";
        }
        return iconString;
    }

    static createJsTree(DOMElementReference, codebase) {
        DOMElementReference.jstree({
            'core': {
                'data': Codebase.getJsTreeJSON(codebase),
                'check_callback': true
            }
        })
    
        DOMElementReference.on('changed.jstree', function(e, data) {
            // console.log(data);
            if(data.selected.length) {
                $(data.selected).each(function(idx) {
                    var node = data.instance.get_node(data.selected[idx]);
                    // console.log('The selected node is ' + node.id);
                    if(data.node.original.type == "file") {
                        // Appear tab
                        var newTab = new Tab({
                            path: node.id,
                            name: node.text
                        })

                        // Appear file content
                        socket.emit("GetFile", node.id);
                    }
                })
            }
        })
    }
    static addJsTreeNode(DOMElementReference, parent, node, position) {
        // DOMElementReference.jstree('create_node', $(parent), { "text":"new_node_text", "id":"new_node_id" }, "first", false, false);
        let parentJQuery = Codebase.convertJsTreeIDToJQueryReadable(parent);
        let parentPathArray = parent.split("/")
        Codebase.openJsTreeFoldersRecursively(DOMElementReference, parentPathArray);
        DOMElementReference.jstree('create_node', $(parentJQuery), node, position, false, false);
    }

    static backSlashToSlash(string) {
        return string.replace(/\\/g, "/");
    }

    static convertJsTreeIDToJQueryReadable(id) {
        return id.replace(/\//g, "\\/");
    }

    static openJsTreeFoldersRecursively(DOMElementReference, pathArray) {
        if(pathArray.length > 0) {
            let currentID = pathArray.join("/");
            pathArray.pop();
            Codebase.openJsTreeFoldersRecursively(DOMElementReference, pathArray);
            DOMElementReference.jstree('open_node', currentID);
        }
    }

    static refreshTree(DOMElementReference, newData) {
        DOMElementReference.jstree(true).settings.core.data = Codebase.getJsTreeJSON(newData);
        DOMElementReference.jstree(true).refresh();
    }
}