var term = new Terminal({
    cursorBlink: "block"
});
Terminal.applyAddon(fit);

term.open(document.getElementById('terminal'));
term.fit();

var currentLine = "";

term.on("data", function(data) {
    // if(event.ctrlKey || event.altKey) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     if(event.keyCode>=65 && event.keyCode<=91)
    //         socket.emit('Terminal', new TextDecoder("utf-8").decode(new Uint8Array([event.keyCode - 64])));
    //     else{
    //         socket.emit('Terminal', new TextDecoder("utf-8").decode(new Uint8Array([keyCode[event.key]])));
    //     }
    // }
    if(data.charCodeAt(0) == 4) {
        alert("Ctrl + D is prohibited!");
    }
    else {
        // Enter
        if(data.charCodeAt(0) == 13) {
            // Prohibit 'screen' command
            if(currentLine[0] == "s" && currentLine[1] == "c" && currentLine[2] == "r" && currentLine[3] == "e" && currentLine[4] == "e" && currentLine[5] == "n") {
                socket.emit("Terminal", "\b\b\b\b\b\b");
                alert("'screen' command is prohibited.")
            }
            socket.emit("Terminal", data)
            currentLine = "";
        } else if(data.charCodeAt(0) == 127) {
            // Backspace
            if(currentLine != "") {
                currentLine = currentLine.substring(0, currentLine.length - 1);
            }
            socket.emit("Terminal", data);
        } 
        else if(data.charCodeAt(0) == 32) {
            // Space
            if(currentLine == "screen") {
                socket.emit("Terminal", "\b\b\b\b\b\b");
                alert("'screen' command is prohibited.")
                currentLine = "";
            }
            else {
                socket.emit("Terminal", data);
            }
        } else {
            if(data.charCodeAt(0) != 27)
                currentLine += data;
            socket.emit("Terminal", data);
        }
    }
})

term.on("paste", function(data) {
    currentLine += data;
    term.write(data);
})

$(window).resize(function(event) {
    term.fit();
    socket.emit("Terminal-resize", {
        row: term.rows,
        col: term.cols
    })
})