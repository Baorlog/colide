let targetPlayground = "http://" + location.hostname + ":1503";
// let targetPlayground = "https://colide.xyz:443";
// let targetPlayground = "localhost:1503";
// let connectionOptions = {
//     "force new connection": true,
//     "reconnectionAttempts": "Infinity",
//     "timeout": 10000,
//     "transports": ["websocket"]
// }

console.log("Connect to " + targetPlayground);
// let socket = io.connect(targetPlayground, connectionOptions);
// let socket = io.connect(targetPlayground);
let socket = io();

let codebaseName = "";

// When connect to server
socket.on('connect', function() {
    codebaseName = localStorage.getItem("currentProjectID");
    // codebaseName = "Project#baolong#project";
    console.log("Connected to server");
    socket.emit("ClientJoin", localStorage.getItem("username"));
    
    // Initiate request codebase
    console.log(`Getting codebase (${codebaseName})...`);
    socket.emit("GetCodebase", codebaseName);

    socket.emit("Terminal-init");

    socket.emit("Terminal-resize", {
        row: term.rows,
        col: term.cols
    })
});
socket.on('connect_timeout', function() {
    console.log("Connection timeout")
});

// When socket disconnect
socket.on('disconnect', function() {
    console.log("Disconnected from server");
});

// When socket try to reconnect
socket.on('reconnect_attempt', function() {
    console.log("Reconnecting...");
});
socket.on('reconnect_error', function() {
    console.log("Reconnect error. Auto try again.");
});
socket.on('reconnect_failed', function() {
    console.log("Reconnect failed. Auto try again.");
});
socket.on('reconnect', function() {
    console.log("Reconnect successfully");
});

// Getting files system
socket.on('GetCodebase', function(codebaseInformation) {
    if(codebaseInformation == "Retry") {
        console.log("Current project ID: " + codebaseName);
        console.log("Getting codebase name again");
        codebaseName = localStorage.getItem("currentProjectID");
        console.log(`Getting codebase (${codebaseName})...`);
        socket.emit("GetCodebase", codebaseName);
    }
    else {
        console.log("Received codebase.");
        console.log(codebaseInformation);
    
        var codebase = new Codebase(codebaseInformation);
    
        var codebaseTree = $("#codebase-tree");
    
        // JSTree
        Codebase.createJsTree(codebaseTree, codebase.codebase);
    }
});

// Get the code of a specific source file
socket.on('GetFile', function(fileInformation) {
    console.log("Received file " + fileInformation.path);
    SourceFile.deleteOldCMEditor(cmInstance);
    cmInstance = new SourceFile(fileInformation.path);
    let isImage = fileInformation.image;
    if(isImage) {
        console.log("File ảnh");
        
        $(".playground").html('<img id="playground-image" width="40%">');
        $("#playground-image").attr("src", fileInformation.content);
    } else {
        cmInstance.initiateCMEditor("codeEditor", localStorage.getItem("editorTheme"), fileInformation.path);
        cmInstance.setValue(fileInformation.content);
        cmInstance.editor.focus();
        cmInstance.editor.setCursor({line: 0, ch: 0});
    }
    currentOpenFile = fileInformation.path;
});

// When other client join
socket.on('ClientJoin', function(newUserInfo) {
    console.log(newUserInfo);
    if(!newUserInfo["exists"]) {
        console.log("User " + newUserInfo.username + " has joined the project.");
        Chat.displayChatNotification("User " + newUserInfo.username + " has joined the project. Welcome!!!");   
    }
    activeConnections[newUserInfo.connect] = new ActiveConnection(newUserInfo.username, newUserInfo.color);
});

// Information about other client's cursor activities
var markers = {};
socket.on('CMCursor', function(otherCursorInformation) {
    let currentFilePathFull = otherCursorInformation["currentFilePath"];
    let currentFilePathArray = currentFilePathFull.split("/");
    let projectID = currentFilePathArray.shift();
    let currentFilePath = currentFilePathArray.join("/");
    activeConnections[otherCursorInformation["client"]].changeOpenFile(currentFilePath);
    if(cmInstance && otherCursorInformation["currentFilePath"] == cmInstance.filePath) {
        if(markers[otherCursorInformation.client] != null) {
            markers[otherCursorInformation.client].clear();
            markers[otherCursorInformation.client] = null;
        }
    
        const cursorCoords = cmInstance.editor.cursorCoords(otherCursorInformation.position);
        const cursorElement = document.createElement('span');
        cursorElement.style.borderLeftStyle = 'solid';
        cursorElement.style.borderLeftWidth = '1px';
        cursorElement.style.borderLeftColor = otherCursorInformation.color;
        cursorElement.style.height = `${(cursorCoords.bottom - cursorCoords.top)}px`;
        cursorElement.style.padding = 0;
        cursorElement.style.zIndex = 0;
    
        markers[otherCursorInformation.client] = cmInstance.editor.setBookmark(otherCursorInformation.position, { widget: cursorElement });
    }
})

// Information about other client's activities on the editor
socket.on('CMEditorChange', function(change) {
    if(cmInstance && change["currentFilePath"] == cmInstance.filePath) {
        cmInstance.editor.replaceRange(change["text"], change["from"], change["to"], "@ignore");
    }
});

// Information about file changes on the server
socket.on('FileChange', function(fileChangeInformation) {
    switch(fileChangeInformation.type) {
        case "new-file":
            console.log(`New file ${fileChangeInformation.path} has been created`);

            let codebaseTree = $("#codebase-tree");

            let parent = "#" + fileChangeInformation.directory.slice(0, -1);

            let fileNameElements = fileChangeInformation.name.split(".");
            let fileExtension = "." + fileNameElements[fileNameElements.length - 1];

            let newNode = { 
                "text": fileChangeInformation.name, 
                "id": fileChangeInformation.path, 
                "icon": Codebase.getFontAwesomeIcon(fileExtension), 
                "type": "file", 
                "state": { 
                    "selected": true 
                } 
            };
            
            codebaseTree.jstree().deselect_all(true);
            Codebase.addJsTreeNode(codebaseTree, parent, newNode, "last");
            socket.emit('GetFile', fileChangeInformation.path);
            break;
        case "new-folder":
            console.log(`New folder ${fileChangeInformation.path} has been created`);
            break;
        case "delete":
            console.log(`${fileChangeInformation.path} has been deleted`);
            break;
        case "rename":
            console.log(`${fileChangeInformation.oldPath} has been renamed to ${fileChangeInformation.newPath}.`);
            break;
        default:
            console.log("Unknown file change information type")
    }
});

// Server's message
socket.on('ServerMessage', function(serverMessage) {
    console.log("Server: " + serverMessage);
});

// For broadcast message
socket.on('Broadcast', function(broadcastMessage) { 
    console.log(broadcastMessage);
});

// When other client left
socket.on('ClientLeft', function(clientLeftInfo) {
    if(markers[clientLeftInfo] != null) {
        markers[clientLeftInfo].clear();
        markers[clientLeftInfo] = null;
    }
    activeConnections[clientLeftInfo].removeActiveConnection();
    Chat.displayChatNotification("User " + activeConnections[clientLeftInfo].username + " has left the project");
    delete activeConnections[clientLeftInfo];
});

// When this client Refresh
socket.on("Refresh", function(codebaseInformation) {
    console.log("Refreshing codebase with new data...");
    console.log(codebaseInformation);

    var codebaseTree = $("#codebase-tree");

    // JSTree
    Codebase.refreshTree(codebaseTree, codebaseInformation)
})

// When this client use Terminal
socket.on("Terminal", function(output) {
    term.write(output);
})

// When this client use Chat
socket.on("Message", function(chatInfo) {
    new Chat(chatInfo);
})
