// var accessToken = null;
// var refreshToken = null;
// var idToken = null;
var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";

function getPastelColor(){ 
    return "hsl(" + 360 * Math.random() + ',' +
               (25 + 70 * Math.random()) + '%,' + 
               (85 + 10 * Math.random()) + '%)'
}

function setUserInfo(userInfos) {
    $(".user-info-text-username").html("User#" + userInfos["Username"]);
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = userInfos["UserAttributes"];
        if(userAttributes.hasOwnProperty("name")) {
            $(".user-info-text-name").html(userAttributes["name"]);
        }
        else {
            $(".user-info-text-name").html(userInfos["Username"]);
        }
        if(userAttributes.hasOwnProperty("email")) {
            $(".user-info-text-email span").html(userAttributes["email"]);
        }
        if(userAttributes.hasOwnProperty("picture")) {
            $(".user-info-avatar-area").html($("<img>", {src:userAttributes["picture"], alt: "avatar", class: "user-info-avatar"}));
            $(".avatar").attr("src", userAttributes["picture"]);
        }
    }
}

function getUserInfo() {
    let userInfos = {
        "Username": localStorage.getItem("username"),
        "UserAttributes": JSON.parse(localStorage.getItem("userAttributes"))
    }
    setUserInfo(userInfos);
}

function setUserFormInfo() {
    $("#username").val(localStorage.getItem("username"));
    let userAttributes = JSON.parse(localStorage.getItem("userAttributes"));
    $("#userEmail").val(userAttributes["email"]);
    if(userAttributes.hasOwnProperty("name")) 
        $("#userFName").val(userAttributes["name"]);
    if(userAttributes.hasOwnProperty("family_name")) 
        $("#userLName").val(userAttributes["family_name"]);
    if(userAttributes.hasOwnProperty("phone_number")) 
        $("#userMobile").val(userAttributes["phone_number"]);
    if(userAttributes.hasOwnProperty("gender")) 
        $("#userGender").val(userAttributes["gender"]);
    if(userAttributes.hasOwnProperty("birthdate")) 
        $("#userDOB").val(userAttributes["birthdate"]);
}

function updateUserAttributes(updateAttributes) {
    $.ajax({
        url: baseURL + "/user/update-user-details",
        type: "POST",
        data: JSON.stringify({
            "accessToken": localStorage.getItem("accessToken"),
            "updateAttributes": updateAttributes
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            redirectTo("/dashboard")
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function uploadAvatar(imageData, callback) {
    $.ajax({
        url: baseURL + "/user/upload-avatar",
        type: "POST",
        data: JSON.stringify({
            "accessToken": localStorage.getItem("accessToken"),
            "username": localStorage.getItem("username"),
            "imageData": imageData
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback(response)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function profileJobs() {
    getUserInfo();
    setUserFormInfo();
}

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

$(document).ready(function() {
    profileJobs();

    $("#userEmail").click(function() {
        $(this).css("border-color", "black");
        $("#userEmailError").css("display", "none");
    })

    $("#userMobile").click(function() {
        $(this).css("border-color", "black");
        $("#userMobileError").css("display", "none");
    })
    
    // Save button
    $("#saveButton").click(function() {
        if($("#userEmail").val() == "") {
            $("#userEmail").css("border-color", "red");
            $("#userEmailError").html("Email cannot be empty");
            $("#userEmailError").css("display", "block");
        }
        else if($("#userMobile").val() != "" && $("#userMobile").val()[0] != "+") {
            $("#userMobile").css("border-color", "red");
            $("#userMobileError").html("Phone number is in wrong format.");
            $("#userMobileError").css("display", "block");
        }
        else {
            let currentUserAttributes = JSON.parse(localStorage.getItem("userAttributes"));

            let userAvatar = document.getElementById("userAvatar");
            var userAvatarData = "";
            
            if(userAvatar.files.length) {
                var userAvatarFile = userAvatar.files[0];
                var reader = new FileReader();
                reader.onload = function(){
                    userAvatarData = reader.result.split(",")[1];

                    let userFName = $("#userFName").val().trim();
                    let userLName = $("#userLName").val().trim();
                    let userEmail = $("#userEmail").val().trim();
                    let userMobile = $("#userMobile").val().trim();
                    let userGender = $("#userGender").val().trim();
                    let userDOB = $("#userDOB").val().trim();
        
                    let updateAttributes = {};
                    uploadAvatar(userAvatarData, function(response) {
                        if(userFName != "" && userFName != currentUserAttributes["name"]) {
                            updateAttributes["name"] = userFName;
                        }
                        if(userLName != "" && userLName != currentUserAttributes["family_name"]) {
                            updateAttributes["family_name"] = userLName;
                        }
                        if(userEmail != "" && userEmail != currentUserAttributes["email"]) {
                            updateAttributes["email"] = userEmail;
                        }
                        if(userMobile != "" && userMobile != currentUserAttributes["phone_number"]) {
                            updateAttributes["phone_number"] = userMobile;
                        }
                        if(userGender != "" && userGender != currentUserAttributes["gender"]) {
                            updateAttributes["gender"] = userGender;
                        }
                        if(userDOB != "" && userDOB != currentUserAttributes["birthdate"]) {
                            updateAttributes["birthdate"] = userDOB;
                        }
                        if(Object.keys(updateAttributes).length){
                            updateUserAttributes(updateAttributes);
                        }
                        else {
                            redirectTo("/dashboard");
                        }
                    })
                };
                reader.readAsDataURL(userAvatarFile);
            }
            else {
                let userFName = $("#userFName").val().trim();
                let userLName = $("#userLName").val().trim();
                let userEmail = $("#userEmail").val().trim();
                let userMobile = $("#userMobile").val().trim();
                let userGender = $("#userGender").val().trim();
                let userDOB = $("#userDOB").val().trim();
    
                let updateAttributes = {};
                if(userFName != "" && userFName != currentUserAttributes["name"]) {
                    updateAttributes["name"] = userFName;
                }
                if(userLName != "" && userLName != currentUserAttributes["family_name"]) {
                    updateAttributes["family_name"] = userLName;
                }
                if(userEmail != "" && userEmail != currentUserAttributes["email"]) {
                    updateAttributes["email"] = userEmail;
                }
                if(userMobile != "" && userMobile != currentUserAttributes["phone_number"]) {
                    updateAttributes["phone_number"] = userMobile;
                }
                if(userGender != "" && userGender != currentUserAttributes["gender"]) {
                    updateAttributes["gender"] = userGender;
                }
                if(userDOB != "" && userDOB != currentUserAttributes["birthdate"]) {
                    updateAttributes["birthdate"] = userDOB;
                }
                if(Object.keys(updateAttributes).length){
                    updateUserAttributes(updateAttributes);
                }
                else {
                    alert("Nothing have changed. Why do wanna save?");
                }
            }
        }
    });
    
    // Cancel button
    $("#cancelButton").click(function() {
        redirectTo("/dashboard");
    });

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
