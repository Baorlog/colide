// var accessToken = null;
// var refreshToken = null;
// var idToken = null;
var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";
var projectID = localStorage.getItem("currentProjectID");
var username = localStorage.getItem("username");
var userID = "User#" + username;
var projectMembers = [];
var projectMemberIDs = [];

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

function setUserInfo(userInfos) {
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = userInfos["UserAttributes"];
        if(userAttributes.hasOwnProperty("picture")) {
            $(".avatar").attr("src", userAttributes["picture"]);
        }
    }
}

function getUserInfo() {
    let userInfos = {
        "Username": localStorage.getItem("username"),
        "UserAttributes": JSON.parse(localStorage.getItem("userAttributes"))
    }
    setUserInfo(userInfos);
}

function getProjectMemberList() {
    $.ajax({
        url: baseURL + "/project/get-members",
        type: "GET",
        data: {
            "projectID": projectID
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            projectMembers = response["Items"];
            response["Items"].forEach(item => {
                projectMemberIDs.push(item["PK"]);
            })
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function newIssueJobs() {
    getUserInfo();
    getProjectMemberList();
}

function getRecommendList(prefix) {
    let initList = projectMemberIDs.slice(0);
    let resultList = [];

    initList.forEach((id, idx, arr) => {
        if(id.includes(prefix)) {
            resultList.push(id);
        }
        if(idx >= arr.length - 1) {
            setRecommendList(resultList);
        }
    })
}

function setRecommendList(itemList) {
    $(".new-issue-form-members-recommend").css("display", "inline-block");
    $(".new-issue-form-members-recommend").html(
        $("<ul>", {class: "new-issue-form-members-recommend-list"})
    );
    itemList.forEach(item => {
        $(".new-issue-form-members-recommend-list").append(
            $("<li>").html(item).click(function() {
                let theChosenOne = $(this).html();
                $(".new-issue-form-members-recommend-list").html("");
                $(".new-issue-form-members-recommend-list").css("display", "none");
                $("#newIssueMember").val(theChosenOne);
            })
        )
    })
}

function addIssue(title, des, deadline, urgency, assign, callback) {
    $.ajax({
        url: baseURL + "/issue/add-issue",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "title": title,
            "description": des,
            "deadline": deadline,
            "urgency": urgency,
            "tag": "",
            "username": username
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            localStorage.setItem("targetIssue", response["Item"]["Title"]);
            if(assign != "") {
                $.ajax({
                    url: baseURL + "/issue/assign-issue",
                    type: "POST",
                    data: JSON.stringify({
                        "projectID": projectID,
                        "issueID": response["Item"]["SK"],
                        "username": assign.split("#")[1]
                    }),
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": localStorage.getItem("idToken")
                    },
                    success: function(response){
                        callback(response);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }
            else {
                callback(response);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


$(document).ready(function() {
    newIssueJobs();

    let currentStep = 1;

    // Recommend assign
    $("#newIssueMember").click(function() {
        getRecommendList($("#newIssueMember").val());
    })
    $("#newIssueMember").on("paste keyup", function() {
        getRecommendList($("#newIssueMember").val());
    })

    // Stop display recommend
    $(document).click(function(event) { 
        var $target = $(event.target);
        if(!$target.closest('#newIssueMember').length && $('.new-issue-form-members-recommend').css("display") != "none") {
            $('.new-issue-form-members-recommend').css("display", "none");
        }        
      });

    // Stop error display
    $("#newIssueTitle").click(function() {
        $("#newIssueTitle").css("border-color", "rgb(175, 175 ,175)");
        $("#issueTitleError").html("");
    });
    $("#newIssueDeadline").click(function() {
        $("#newIssueDeadline").css("border-color", "rgb(175, 175 ,175)");
        $("#issueDeadlineError").html("");
    });

    // Form button
    $(".btn.next").click(function() {
        if(currentStep == 1 && $("#newIssueTitle").val().trim() == ""){
            $("#newIssueTitle").css("border-color", "red");
            $("#issueTitleError").html("This field cannot be empty");
        }
        else if(currentStep == 1 && $("#newIssueDeadline").val().trim() == "") {
            $("#newIssueDeadline").css("border-color", "red");
            $("#issueDeadlineError").html("This field cannot be empty");
        }
        else {
            if($("#newIssueTitle").val() != "")
                $("#new-issue-form-review-title").html($("#newIssueTitle").val());
            if($("#newIssueDescription").val() != "")
                $("#new-issue-form-review-description").html($("#newIssueDescription").val());
            if($("#newIssueDeadline").val() != "")
                $("#new-issue-form-review-deadline").html($("#newIssueDeadline").val());
            if($("#newIssueUrgency").val() != "")
                $("#new-issue-form-review-urgency").html($("#newIssueUrgency").val());
            if($("#newIssueMember").val() != "")
                $("#new-issue-form-review-assign").html($("#newIssueMember").val());

            $("#new-issue-steps-item-" + currentStep).removeClass("active").addClass("inactive");
            $("#new-issue-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
            $(".new-issue-form-" + currentStep).css("display", "none");
            $(".new-issue-form-" + (currentStep + 1)).css("display", "block");
            currentStep++;
        }
    });
    $(".btn.cancel").click(function() {
        redirectTo("/project");
    });
    $(".btn.previous").click(function() {
        $("#new-issue-steps-item-" + currentStep).removeClass("active").addClass("inactive");
        $("#new-issue-steps-item-" + (currentStep - 1)).removeClass("inactive").addClass("active");
        $(".new-issue-form-" + currentStep).css("display", "none");
        $(".new-issue-form-" + (currentStep - 1)).css("display", "block");
        currentStep--;
    });
    $(".btn.finish").click(function() {

        // Loader
        $(".new-issue-form-entry div.loader").css("display", "inline-block");

        // Get form info
        let issueTitle = $("#newIssueTitle").val().trim();
        let issueDescription = $("#newIssueDescription").val();
        let issueDeadline = $("#newIssueDeadline").val().trim();
        let issueUrgency = $("#newIssueUrgency").val();
        let issueAssign = "";
        if($("#newIssueMember").val() != "")
            issueAssign = $("#newIssueMember").val();

        addIssue(issueTitle, issueDescription, issueDeadline, issueUrgency, issueAssign, function(response) {
            redirectTo("/project");
        })

    });

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
