// var accessToken = null;
// var refreshToken = null;
// var idToken = null;
var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";
var chosenMemberArray = [];
var createdProjects = [];

function setUserInfo(userInfos) {
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = userInfos["UserAttributes"];
        if(userAttributes.hasOwnProperty("picture")) {
            $(".avatar").attr("src", userAttributes["picture"]);
        }
    }
}

function getUserInfo() {
    let userInfos = {
        "Username": localStorage.getItem("username"),
        "UserAttributes": JSON.parse(localStorage.getItem("userAttributes"))
    }
    setUserInfo(userInfos);
}

function newProjectJobs() {
    getUserInfo();
    getCreatedProjects();
}

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

function setRecommendListLoading() {
    $(".new-project-form-members-recommend").css("display", "inline-block");
    $(".new-project-form-members-recommend").html(
        $("<ul>", {class: "new-project-form-members-recommend-list"}).html(
            $("<li>")
        )
    );
}

function setRecommendList(itemList) {
    $(".new-project-form-members-recommend").css("display", "inline-block");
    $(".new-project-form-members-recommend").html(
        $("<ul>", {class: "new-project-form-members-recommend-list"})
    );
    itemList.forEach(item => {
        $(".new-project-form-members-recommend-list").append(
            $("<li>").html(item["PK"]).click(function() {
                let theChosenOne = $(this).html();
                $(".new-project-form-members-recommend-list").html("");
                $(".new-project-form-members-recommend-list").css("display", "none");
                $("#newProjectMember").val("");
        
                if(!chosenMemberArray.includes(theChosenOne)) {
                    chosenMemberArray.push(theChosenOne);
                    $(".new-project-members-usernames").append(
                        $("<div>", {class: "new-project-members-username", id: "new-project-members-username-" + theChosenOne}).html([
                            theChosenOne,
                            $("<span>", {text: "×"}).click(function() {
                                $(this).parent().remove();
                                const idx = chosenMemberArray.indexOf(theChosenOne);
                                if (idx > -1) {
                                    chosenMemberArray.splice(idx, 1);
                                }
                            })
                        ])
                    )
                }
            })
        )
    })
}

function getCorrectedList(fullList, callback) {
    console.log(fullList);
    fullList.forEach((item, index, array) => {
        if(chosenMemberArray.includes(item["PK"])){
            const idx = fullList.indexOf(item);
            if (idx > -1) {
                fullList.splice(idx, 1);
            }
        }
        if(index == array.length - 1) {
            console.log(fullList);
            callback(fullList);
        }
    })
}

function getRecommendUserList(prefix) {
    $.ajax({
        url: baseURL + "/user/search-user-list",
        type: "GET",
        data: {
            "usernamePrefix": prefix
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        beforeSend: function(xhr) {
            setRecommendListLoading();
        },
        success: function(response){
            // getCorrectedList(response["Items"], setRecommendList);
            setRecommendList(response["Items"]);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function getCreatedProjects() {
    $.ajax({
        url: baseURL + "/project/get-created-projects",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            createdProjects = response["Items"];
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function checkProjectNameExists(newProjectName, callback) {
    let exists = false;
    if(createdProjects.length) {
        createdProjects.forEach((item, idx, arr) => {
            if(item["SK"].split("#")[2] == newProjectName) {
                exists = true;
            }
            if(idx == arr.length - 1) {
                callback(exists);
            }
        })
    }
    else {
        callback(exists);
    }
}

function addWorkingProject(username, projectID, callback) {
    if(username == localStorage.getItem("username")) {
        callback()
    } else {
        $.ajax({
            url: baseURL + "/project/add-working-project",
            type: "POST",
            data: JSON.stringify({
                "username": username,
                "projectID": projectID,
                "role": "Guest"
            }),
            headers: {
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem("idToken")
            },
            success: function(response){
                callback()
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}

function createNewServer(projectID, codebaseChoice, instanceType, codebaseSource, callback) {
    $.ajax({
        url: baseURL + "/server/create-new-server",
        type: "POST",
        data: JSON.stringify({
            "projectID": projectID,
            "codebase": codebaseChoice,
            "instanceType": instanceType,
            "codebaseSource": codebaseSource
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            callback();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function createNewProject(projectName, projectDescription, codebaseChoice, codebaseSource, instanceType) {
    let creator = localStorage.getItem("username")
    $.ajax({
        url: baseURL + "/project/create-new-project",
        type: "POST",
        data: JSON.stringify({
            "username": creator,
            "projectName": projectName,
            "description": projectDescription,
            "codebase": codebaseChoice,
            "codebaseSource": codebaseSource
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            createNewServer("Project#" + creator + "#" + projectName, codebaseChoice, instanceType, codebaseSource, function() {
                chosenMemberArray.forEach((member, idx, arr) => {
                    addWorkingProject(member.split("#")[1], "Project#" + creator + "#" + projectName, function() {
                        if(idx == arr.length - 1) {
                            redirectTo("/dashboard");
                        }
                    })
                })
            });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

$(document).ready(function() {
    newProjectJobs();

    let currentStep = 1;
    let codebaseChoice = "New";
    let username = localStorage.getItem("username");
    let userID = "User#" + username;

    // Include this user in project member list
    chosenMemberArray.push(userID);
    $(".new-project-members-usernames").append(
        $("<div>", {class: "new-project-members-username", id: "new-project-members-username-" + userID}).html([
            userID
        ])
    )

    // Unerror project name input
    $("#newProjectName").click(function() {
        $(this).css("border-color", "rgb(175, 175, 175)");
        $("#projectNameError").html("");
    })

    // Codebase type choose
    $("#codebaseNew").click(function() {
        codebaseChoice = "New";
        $("#projectCodebaseError").html("");
        if(!$(this).hasClass("active")) {
            if($("#codebaseGit").hasClass("active"))
                $($("#codebaseGit")).removeClass("active");
            else if($("#codebaseUpload").hasClass("active"))
                $($("#codebaseUpload")).removeClass("active");
            $(this).addClass("active");
            $(".new-project-form-entry label").css("display", "none");
            $("#newProjectCodebase").attr("type", "hidden");
        }
    });

    $("#codebaseGit").click(function() {
        codebaseChoice = "Git";
        $("#projectCodebaseError").html("");
        if(!$(this).hasClass("active")) {
            if($("#codebaseNew").hasClass("active"))
                $($("#codebaseNew")).removeClass("active");
            else if($("#codebaseUpload").hasClass("active"))
                $($("#codebaseUpload")).removeClass("active");
            $(this).addClass("active");
            $(".new-project-form-entry label").css("display", "inline").html("Git repository:");
            $("#newProjectCodebase").attr("type", "text");
        }
    });

    $("#codebaseUpload").click(function() {
        codebaseChoice = "Upload";
        $("#projectCodebaseError").html("");
        if(!$(this).hasClass("active")) {
            if($("#codebaseNew").hasClass("active"))
                $($("#codebaseNew")).removeClass("active");
            else if($("#codebaseGit").hasClass("active"))
                $($("#codebaseGit")).removeClass("active");
            $(this).addClass("active");
            $(".new-project-form-entry label").css("display", "inline").html("Upload .zip file:");
            $("#newProjectCodebase").attr("type", "file");
        }
    });

    let memberTimeout = null;
    // Member list recommend
    $("#newProjectMember").on("paste keyup", function() {
        if(memberTimeout != null) {
            clearTimeout(memberTimeout);
        }
        memberTimeout = setTimeout(function() {
            if($("#newProjectMember").val() != "") {
                getRecommendUserList($("#newProjectMember").val());
            } else {
                $(".new-project-form-members-recommend").css("display", "none");
            }
            memberTimeout = null;
        }, 1000);
    });

    // Form button
    $(".btn.next").click(function() {
        if(currentStep == 1 && $("#newProjectName").val().trim() == ""){
            $("#newProjectName").css("border-color", "red");
            $("#projectNameError").html("This field cannot be empty");
        }
        else if(currentStep == 1 && $("#newProjectName").val().trim() != ""){
            if($("#newProjectName").val().trim().includes("#")) {
                $("#newProjectName").css("border-color", "red");
                $("#projectNameError").html("Project name cannot have '#' character.");
            }
            else {
                checkProjectNameExists($("#newProjectName").val().trim(), function(exists) {
                    if(exists) {
                        $("#newProjectName").css("border-color", "red");
                        $("#projectNameError").html("Project name is already exists.");
                    }
                    else {
                        $("#new-project-form-review-name").html($("#newProjectName").val().trim());
                        $("#new-project-form-review-description").html($("#newProjectDescription").val());
                        
                        $("#new-project-steps-item-" + currentStep).removeClass("active").addClass("inactive");
                        $("#new-project-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
                        $(".new-project-form-" + currentStep).css("display", "none");
                        $(".new-project-form-" + (currentStep + 1)).css("display", "block");
                        currentStep++;
                    }
                })
            }
        }
        else if(currentStep == 2 && (codebaseChoice == "Git" && $("#newProjectCodebase").val() == "") || (codebaseChoice == "Upload" && $("#newProjectCodebase").val() == "")) {
            $("#projectCodebaseError").html("Please enter project source codebase");
        }
        else if (currentStep == 2) {
            if(codebaseChoice == "New")
                $("#new-project-form-review-codebase").html("Create new empty project")
            else if(codebaseChoice == "Git")
                $("#new-project-form-review-codebase").html("Clone from Git repository")
            else if(codebaseChoice == "Upload")
                $("#new-project-form-review-codebase").html("Upload from computer")

            $("#new-project-form-review-instanceType").html($("#newProjectInstanceType").val());
                
            $("#new-project-steps-item-" + currentStep).removeClass("active").addClass("inactive");
            $("#new-project-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
            $(".new-project-form-" + currentStep).css("display", "none");
            $(".new-project-form-" + (currentStep + 1)).css("display", "block");
            currentStep++;
        }
        else if (currentStep == 3) {
            chosenMemberArray.forEach((member, idx, arr) => {
                $("#new-project-form-review-members ul").append(
                    $("<li>", {text: member})
                )
                if(idx == arr.length - 1) {
                    $("#new-project-steps-item-" + currentStep).removeClass("active").addClass("inactive");
                    $("#new-project-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
                    $(".new-project-form-" + currentStep).css("display", "none");
                    $(".new-project-form-" + (currentStep + 1)).css("display", "block");
                    currentStep++;
                }
            })
        }
        else {
            $("#new-project-steps-item-" + currentStep).removeClass("active").addClass("inactive");
            $("#new-project-steps-item-" + (currentStep + 1)).removeClass("inactive").addClass("active");
            $(".new-project-form-" + currentStep).css("display", "none");
            $(".new-project-form-" + (currentStep + 1)).css("display", "block");
            currentStep++;
        }
    });
    $(".btn.cancel").click(function() {
        redirectTo("/dashboard");
    });
    $(".btn.previous").click(function() {
        $("#new-project-steps-item-" + currentStep).removeClass("active").addClass("inactive");
        $("#new-project-steps-item-" + (currentStep - 1)).removeClass("inactive").addClass("active");
        $(".new-project-form-" + currentStep).css("display", "none");
        $(".new-project-form-" + (currentStep - 1)).css("display", "block");
        currentStep--;
    });
    $(".btn.finish").click(function() {

        // Loader
        $(".new-project-form-entry div.loader").css("display", "inline-block");

        // Step 1: Project information
        let projectName = $("#newProjectName").val().trim();
        let projectDescription = $("#newProjectDescription").val();
        let codebaseSource = null;

        // Step 2: Server
        if (codebaseChoice == "Git") {
            codebaseSource = $("#newProjectCodebase").val();
        } 
        else if (codebaseChoice == "Upload") {

            var reader = new FileReader();
            
            reader.readAsText(document.getElementById("newProjectCodebase").files[0]);
    
            reader.onload = function() {
                console.log(reader.result);
            }

        }

        let instanceType = $("#newProjectInstanceType").val();

        // Step 3: Members
        // Get from variable array chosenMemberArray

        // Step 4: Review

        // Complete
        createNewProject(projectName, projectDescription, codebaseChoice, codebaseSource, instanceType);

    });

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
