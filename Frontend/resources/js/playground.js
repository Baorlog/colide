// Global variables

var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";

// Initiate CodeMirror Editor
// var cmInstance = new SourceFile("codeEditor", "new.js", "darcula");
var cmInstance = null;

// Project table
var projectTableIsUp = true;

// Toolbar menu status
var toolbarIsUp = true;

// Current open toolbar menu
var currentToolbarMenu = null;

// Open tabs
var openTabs = {};

// Current open file
var currentOpenFile = null;

// Chat display status
var chatIsOn = true;

// Active connections
var activeConnections = {};

// Active connections display status
var activeConnectionsIsOn = true;

// Reusable functions

function closeToolbar() {
    $(".toolbar").css("margin-top", "0px");
    $(".leftbar").css("margin-top", "4vh");
    $(".playground").css("margin-top", "4vh");
    $(".rightbar").css("margin-top", "4vh");
    toolbarIsUp = true;
}

function showToolbar() {
    $(".toolbar").css("margin-top", "4vh");
    $(".leftbar").css("margin-top", "6.5vh");
    $(".playground").css("margin-top", "6.5vh");
    $(".rightbar").css("margin-top", "6.5vh");
    toolbarIsUp = false;
}

function redirectToDomain(domain) {
    window.location.href = domain;
}

function setUserAva() {
    let userAttributes = JSON.parse(localStorage.getItem("userAttributes"))
    if(userAttributes.hasOwnProperty("picture")) {
        $(".avatar").attr("src", userAttributes["picture"].replace(/\#/g, "%23"));
    }
    if(userAttributes.hasOwnProperty("custom:editorTheme")) {
        localStorage.setItem("editorTheme", userAttributes["custom:editorTheme"]);
    }
}

function getProjectServer(projectID, callback) {
    $.ajax({
        url: baseURL + "/project/get-servers",
        type: "GET",
        data: {
            "projectID": projectID
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            if(response["Items"].length) {
                $.ajax({
                    url: baseURL + "/server/get-server",
                    type: "GET",
                    data: {
                        "projectID": projectID,
                        "serverID": response["Items"][0]["SK"]
                    },
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": localStorage.getItem("idToken")
                    },
                    success: function(response){
                        callback(response["InstanceInfo"]["PublicIpAddress"])
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if(XMLHttpRequest["responseJSON"] == "Oops, sorry! We can't find any server.") {
                            $("#project-view-servers-error").html(XMLHttpRequest["responseJSON"]);
                        }
                    }
                });
            }
            else {
                alert("Err: Target project has 0 server running");
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
            if(XMLHttpRequest["responseJSON"] == "Oops, sorry! We can't find any server.") {
                $("#project-view-servers-error").html(XMLHttpRequest["responseJSON"]);
            }
        }
    });
}

function goToProject(projectInfo) {
    getProjectServer(projectInfo["SK"], (serverIP) => {
        localStorage.setItem("targetPlayground", serverIP);
        localStorage.setItem("currentProjectID", projectInfo["SK"]);
        location.reload();
    });
}

function displayProjectList(projectList) {
    if(projectList.length) {
        projectList.forEach((project, idx, arr) => {
            $(".project-table-selector-list").append(
                $("<li>").html(
                    $("<button>", {class: "project-table-selector-item btn"}).html([
                        $("<div>", {class: "col span-1-of-5"}).html(
                            $("<div>", {class: "project-table-selector-list-logo", text: project["SK"].split("#")[2][0]})
                        ),
                        $("<div>", {class: "col span-3-of-5"}).html([
                            $("<div>", {class: "project-table-selector-list-name", text: project["SK"].split("#")[2]}),
                            $("<div>", {class: "project-table-selector-list-author", text: "Author: " + project["SK"].split("#")[1]})
                        ]),
                        $("<div>", {class: "col span-1-of-5"}).html(
                            $("<div>", {class: "project-table-selector-list-status", text: function() {
                                if(project["SK"] == localStorage.getItem("currentProjectID")) {
                                    return "Current";
                                }
                            }})
                        )
                    ]).click(function() {
                        if(project["SK"] != localStorage.getItem("currentProjectID")) {
                            $(this).children().last().children().first().html("Going...");
                            goToProject(project);
                        }
                    })
                )
            )
        })
    }
    else {
        $(".project-table-selector-notification").css("display", "block");
    }
}

function displayWorkingProjects() {
    $.ajax({
        url: baseURL + "/project/get-working-projects",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        beforeSend: function() {
            $(".project-table-selector-loading").css("display", "block");
            $(".project-table-selector-notification").css("display", "none");
            $(".project-table-selector-list").html("");
        },
        success: function(response){
            $(".project-table-selector-loading").css("display", "none");
            displayProjectList(response["Items"]);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function displayCreatedProjects() {
    $.ajax({
        url: baseURL + "/project/get-created-projects",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        beforeSend: function() {
            $(".project-table-selector-loading").css("display", "block");
            $(".project-table-selector-notification").css("display", "none");
            $(".project-table-selector-list").html("");
        },
        success: function(response){
            $(".project-table-selector-loading").css("display", "none");
            displayProjectList(response["Items"]);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function displayStarredProjects() {
    $.ajax({
        url: baseURL + "/project/get-starred-projects",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        beforeSend: function() {
            $(".project-table-selector-loading").css("display", "block");
            $(".project-table-selector-notification").css("display", "none");
            $(".project-table-selector-list").html("");
        },
        success: function(response){
            $(".project-table-selector-loading").css("display", "none");
            displayProjectList(response["Items"]);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function displayProjectsInGroup(groupName) {
    if(groupName == "Working projects") {
        displayWorkingProjects();
    }
    else if(groupName == "Created projects") {
        displayCreatedProjects();
    }
    else if(groupName == "Starred projects") {
        displayStarredProjects();
    }
}

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

function getParams(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var value = url.searchParams.get(param);
    return value;
}

$(document).ready(function(){

    // Set init information
    let paramUsername = getParams("username");
    let paramUserAttributes = getParams("userAttributes");
    let paramIdToken = getParams("idToken");
    let paramCurrentProjectID = getParams("currentProjectID");
    if(paramUsername && paramUserAttributes && paramIdToken && paramCurrentProjectID){
        localStorage.setItem("username", paramUsername);
        localStorage.setItem("userAttributes", paramUserAttributes);
        localStorage.setItem("idToken", paramIdToken);
        localStorage.setItem("currentProjectID", paramCurrentProjectID.replace(/\%23/g, "#"));
        window.history.pushState({},"", location.protocol + "//" + location.hostname + ":" + location.port + location.pathname);
        setUserAva();
    }
    else {
        setUserAva();
    }

    /**
     * Logo
     */

    $(".header-logo a").click(function() {
        redirectToDomain("https://colide.xyz/dashboard");
    })

    /**
     * Project Selector
     */
    $(".project-btn").html(localStorage.getItem("currentProjectID").split("#")[2]);
    $(".project-btn").click(function() {
        if(projectTableIsUp) {
            projectTableIsUp = false;
            $(".project-table").css("display", "block");
        }
        else {
            projectTableIsUp = true;
            $(".project-table").css("display", "none");
        }
    })
     
    displayWorkingProjects();

    $(".project-table-group-list li button").click(function() {
        $(".project-table-group-list li button.active").removeClass("active").addClass("inactive").prop("disabled", false);
        $(this).removeClass("inactive").addClass("active").prop("disabled", true);
        $(".project-table").css("display", "block");

        var groupName = $(this).html();

        // Appear project in group
        displayProjectsInGroup(groupName);
    })
    


    /**
     * Toolbar
     */

    // Toolbar menu dropdown
    $(".header-action-button").click(() => {
        if(toolbarIsUp) {
            showToolbar();
        }
        else {
            closeToolbar();
            if(currentToolbarMenu != null) {
                currentToolbarMenu.next().css("display", "none");
                currentToolbarMenu = null;
            }
        }
    });

    // Toolbar dropdown dropdown
    $(".toolbar-menu-btn").click(function(){
        if(currentToolbarMenu == null) {
            currentToolbarMenu = $(this);
            currentToolbarMenu.next().css("display", "block");
        } else {
            currentToolbarMenu.next().css("display", "none");
            if($(this).is(currentToolbarMenu)) {
                currentToolbarMenu = null;
            } else {
                currentToolbarMenu = $(this);
                currentToolbarMenu.next().css("display", "block");
            }
        }
    });
    $(".toolbar-menu-btn").mouseover(function() {
        if(currentToolbarMenu != null) {
            if(!$(this).is(currentToolbarMenu)) {
                currentToolbarMenu.next().css("display", "none");
                currentToolbarMenu = $(this);
                currentToolbarMenu.next().css("display", "block");
            }
        }
    })

    // Close when click outside
    $(document).click(function(event) {
        if (!$(event.target).closest(".toolbar-menu-btn").length) {
            if(currentToolbarMenu != null) {
                currentToolbarMenu.next().css("display", "none");
                currentToolbarMenu = null;
            }
        }
    });

    // Colide -> New Project
    $("#toolbar-menu-dropdown-btn-colide-new-project").click(function() {
        redirectToDomain("https://colide.xyz/new-project");
    })

    // Colide -> Open Project
    $("#toolbar-menu-dropdown-btn-colide-open-project").click(function() {
        redirectToDomain("https://colide.xyz/dashboard");
    })

    // Colide -> Members
    $("#toolbar-menu-dropdown-btn-colide-members").click(function() {
        // localStorage.setItem("targetProjectView", "Members");
        redirectToDomain("https://colide.xyz/project?view=Members");
    })

    //File -> New File
    $("#toolbar-menu-dropdown-btn-file-new-file").click(function() {
        // Create the modal box
        var newModal = new ModalBox($("#modalBox"), "New File");

        // Get the modal
        $("#modalBox").css("display", "block");
        closeToolbar();
    })

    //File -> New Folder
    $("#toolbar-menu-dropdown-btn-file-new-folder").click(function() {
        // Create the modal box
        var newModal = new ModalBox($("#modalBox"), "New Folder");

        // Get the modal
        $("#modalBox").css("display", "block");
        closeToolbar();
    })

    //File -> Save
    $("#toolbar-menu-dropdown-btn-file-save").click(function() {
        if(cmInstance != null) {
            var saveContent = cmInstance.getValue();
            socket.emit('FileChange', {
                "type": "save",
                "path": cmInstance.filePath,
                "content": saveContent
            });
            console.log("Save Successfully");
            alert("Save Successfully");
            // $.notify("Save Successfully");
        } else {
            alert("There is no file opened");
        }
    })

    //File -> Delete
    $("#toolbar-menu-dropdown-btn-file-delete").click(function() {
        let selectedFiles = $("#codebase-tree").jstree("get_selected");
        let selectedFilesInformation = [];
        selectedFiles.forEach((fileID, idx, arr) => {
            selectedFilesInformation.push({
                "id": fileID,
                "fileType": $("#codebase-tree").jstree(true).get_node(fileID).original.type
            })

            if(idx == arr.length - 1) {
                // Create the modal box
                var newModal = new ModalBox($("#modalBox"), "Delete", [selectedFiles, selectedFilesInformation]);
        
                // Get the modal
                $("#modalBox").css("display", "block");
                closeToolbar();
            }
        });
    });

    // File -> Rename
    $("#toolbar-menu-dropdown-btn-file-rename").click(function() {
        let selectedFile = $("#codebase-tree").jstree("get_selected");
        if(selectedFile.length == 1) {
            // Create the modal box
            var newModal = new ModalBox($("#modalBox"), "Rename", selectedFile);
    
            // Get the modal
            $("#modalBox").css("display", "block");
            closeToolbar();
        } else {
            alert("Please choose only 1 file to rename.")
        }
    })

    // File -> Refresh
    $("#toolbar-menu-dropdown-btn-file-refresh").click(function() {
        socket.emit("Refresh", localStorage.getItem("currentProjectID"));
    })

    // Edit -> Cut
    $("#toolbar-menu-dropdown-btn-edit-cut").click(function() {
        if(cmInstance == null) {
            alert("No file is opened");
        }
        else {
            if(cmInstance.editor.getSelection().length) {
                document.execCommand("cut");
                console.log(cmInstance.editor.replaceSelection(""));
            }
        }
    })

    // Edit -> Copy
    $("#toolbar-menu-dropdown-btn-edit-copy").click(function() {
        if(cmInstance == null) {
            alert("No file is opened");
        }
        else {
            if(cmInstance.editor.getSelection().length) {
                document.execCommand("copy");
            }
        }
    })

    // Edit -> Paste
    $("#toolbar-menu-dropdown-btn-edit-paste").click(function() {
        if(cmInstance == null) {
            alert("No file is opened");
        }
        else {
            var cursorPos = cmInstance.editor.getCursor();
            navigator.clipboard.readText()
                .then(text => {
                    cmInstance.editor.replaceRange(text, {line: cursorPos.line, ch: cursorPos.ch}, {line: cursorPos.line, ch: cursorPos.ch});
                })
                .catch(err => {
                    console.error('Failed to read clipboard contents: ', err);
                });
        }
    })

    // Edit -> Undo
    $("#toolbar-menu-dropdown-btn-edit-undo").click(function() {
        if(cmInstance == null) {
            alert("No file is opened");
        }
        else {
            cmInstance.editor.undo();
        }
    })

    // Edit -> Redo
    $("#toolbar-menu-dropdown-btn-edit-redo").click(function() {
        if(cmInstance == null) {
            alert("No file is opened");
        }
        else {
            cmInstance.editor.redo();
        }
    })

    // Editor -> Theme
    $("#toolbar-menu-dropdown-btn-editor-theme").click(function() {
        // Create the modal box
        var newModal = new ModalBox($("#modalBox"), "Theme");
    
        // Get the modal
        $("#modalBox").css("display", "block");
        closeToolbar();
    })

    // Editor -> Comment
    $("#toolbar-menu-dropdown-btn-editor-comment").click(function() {
        cmInstance.editor.execCommand('toggleComment')
    })

    // Git -> Clone
    $("#toolbar-menu-dropdown-btn-git-clone").click(function() {
        socket.emit("Terminal", "git clone ");
    })

    // Git -> Checkout
    $("#toolbar-menu-dropdown-btn-git-checkout").click(function() {
        socket.emit("Terminal", "git checkout ");
    })

    // Git -> Branch
    $("#toolbar-menu-dropdown-btn-git-branch").click(function() {
        socket.emit("Terminal", "git branch\n");
    })

    // Git -> Add
    $("#toolbar-menu-dropdown-btn-git-add").click(function() {
        socket.emit("Terminal", "git add .");
    })

    // Git -> Commit
    $("#toolbar-menu-dropdown-btn-git-commit").click(function() {
        socket.emit("Terminal", "git commit -m \"\"");
    })

    // Git -> Push
    $("#toolbar-menu-dropdown-btn-git-push").click(function() {
        socket.emit("Terminal", "git push\n");
    })

    // Git -> Pull
    $("#toolbar-menu-dropdown-btn-git-pull").click(function() {
        socket.emit("Terminal", "git pull\n");
    })

    // Git -> Merge
    $("#toolbar-menu-dropdown-btn-git-merge").click(function() {
        socket.emit("Terminal", "git merge ");
    })

    // Git -> Rebase
    $("#toolbar-menu-dropdown-btn-git-rebase").click(function() {
        socket.emit("Terminal", "git rebase ");
    })

    // Terminal -> New Terminal
    $("#toolbar-menu-dropdown-btn-terminal-new").click(function() {
        socket.emit("Terminal-init");
    })

    // Communication -> Chat
    $("#toolbar-menu-dropdown-btn-communication-chat").click(function() {
        if(chatIsOn) {
            chatIsOn = false;
            $(".rightbar").css("display", "none");
            $(".playground").css("width", "88%");
            $("#terminal").css("width", "88%");
            term.fit();
            socket.emit("Terminal-resize", {
                row: term.rows,
                col: term.cols
            })
        }
        else {
            chatIsOn = true;
            $(".rightbar").css("display", "block");
            $(".playground").css("width", "76%");
            $("#terminal").css("width", "76%");
            term.fit();
            socket.emit("Terminal-resize", {
                row: term.rows,
                col: term.cols
            })
        }
    })

    // Communication -> Active connections
    $("#toolbar-menu-dropdown-btn-communication-activeconnections").click(function() {
        if(activeConnectionsIsOn) {
            activeConnectionsIsOn = false;
            $(".active-connections").css("display", "none");
            $(".codebase-area").css("height", "100%");
        }
        else {
            activeConnectionsIsOn = true;
            $(".active-connections").css("display", "block");
            $(".codebase-area").css("height", "60%");
        }
    })


    $("#refresh").click(function() {
        socket.emit("Refresh", localStorage.getItem("currentProjectID"));
    })

    // Chat function
    // Welcome chat
    $("#chatProject").html(localStorage.getItem("currentProjectID"));
    // Normal send button
    $("#chatBtn").click(function() {
        var newMessage = $("#chatInput").val();
        $("#chatInput").val("");
        socket.emit("Message", {
            author: localStorage.getItem("username"),
            content: newMessage
        })
        // Chat.displayChatNotification("Hello???");
    })
    // Ctrl+Enter when typing chat
    $('#chatInput').keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            var newMessage = $("#chatInput").val();
            $("#chatInput").val("");
            socket.emit("Message", {
                author: localStorage.getItem("username"),
                content: newMessage
            })
        }
    });

    // Profile button
    $("#profile-btn").click(function() {
        redirectToDomain("https://colide.xyz/profile");
    })

    // Logout button
    $("#logout-btn").click(function() {
        // localStorage.removeItem("accessToken");
        // localStorage.removeItem("refreshToken");
        // localStorage.removeItem("idToken");
        // localStorage.removeItem("username");
        // localStorage.removeItem("userAttributes");
        redirectToDomain("https://colide.xyz/dashboard?logout=true");
    })
});