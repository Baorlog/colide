/**
 * Configuration CodeMirror
 */
var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
    mode: "javascript",
    theme: "darcula",
    lineNumbers: true,
    matchBrackets: true,
    indentUnit: 4,
    indentWithTabs: true,
    // autoCloseTags: true,
    autoCloseBrackets: true,
    // matchTags: false,
    extraKeys: {"Ctrl-Space": "autocomplete"}
    // gutters: ["CodeMirror-linenumbers", "breakpoints"]
});

editor.setSize("100%","71vh");


/**
 * Auto complete
 */

function isValidForAutoComplete(keyCode) {
    restrictKeys = [
        8,      //Backspace
        13,     //Return
        17,     //Ctrl
        18,     //Alt
        32,     //Space
        37,     //Left arrow
        38,     //Up arrow
        39,     //Right arrow
        40,     //Down arrow
        // 46,     //Delete
        58,     //Colon
        59,     //Semi-colon
        91,     //[
        93,     //]
        123,    //{
        125,    //}
        127,    //Delete
        219,    //[{
        221     //]}
    ];
    if(restrictKeys.includes(keyCode)) {
        return false;
    }
    else {
        return true;
    }
}

editor.on("keypress", function (cm, event) {
    if (!cm.state.completionActive && /*Enables keyboard navigation in autocomplete list*/
        // event.keyCode != 13 && event.keyCode != 8 && event.keyCode != 46) {         
        isValidForAutoComplete(event.keyCode)) {         
        CodeMirror.commands.autocomplete(cm, null, {completeSingle: false});
    }
});


/**
 * Event listeners
 */

editor.on("change", function(cm, event) {
    if(event.origin != "@ignore") {
        socket.emit('CMEditorChange', event);
    }
})

editor.on("cursorActivity", function(cm) {
    var cursorPos = editor.getCursor();
    socket.emit('CMCursor', cursorPos);
})

editor.on('blur', function () { 
    $(".CodeMirror-cursors").css('visibility', 'visible'); 
});

// editor.on("gutterClick", function(cm, n) {
//     var info = cm.lineInfo(n);
//     cm.setGutterMarker((n, "breakpoints", info.gutterMarkers ? null : makeMarker());
//   });
  
// function makeMarker() {
// var marker = document.createElement("div");
// marker.style.color = "#822";
// marker.innerHTML = "●";
// return marker;
// }

// document.getElementById("save").onclick = function() {
//     var textToWrite = editor.getValue();
//     console.log(textToWrite);
// }

// document.getElementById("changeTheme").onchange= selectTheme;

// var input = document.getElementById("changeTheme");
// function selectTheme() {
//     var theme = input.options[input.selectedIndex].textContent;
//     editor.setOption("theme", theme);
//     location.hash = "#" + theme;
// }

// var choice = (location.hash && location.hash.slice(1)) ||
//             (document.location.search &&
//             decodeURIComponent(document.location.search.slice(1)));
// if (choice) {
//     input.value = choice;
//     editor.setOption("theme", choice);
// }
// CodeMirror.on(window, "hashchange", function() {
//     var theme = location.hash.slice(1);
//     if (theme) { 
//         input.value = theme; 
//         selectTheme(); 
//     }
// });