// $(document).ready(function() {
//     $(".data").jstree({
//         // "plugins": ["checkbox"]
//     })

//     $(".data").on('changed.jstree', function(e, data) {
//         console.log(data);
//         if(data.selected.length) {
//             $(data.selected).each(function(idx) {
//                 var node = data.instance.get_node(data.selected[idx]);
//                 console.log('The selected node is ' + node.text);
//             })
//         }
//     })
// });
// Setup the jsTree.
$(function () {
    $('#jstree').jstree({
        'core': {
            'data': [{
                "id": "base_directory",
                "text": "Base Directory",
                "state": {
                    "opened": true
                },
                "children": [{
                    "text": "Sub 1",
                    "id": "sub_1"
                }]
            }],
            'check_callback': true
        }
    });
});

// When the jsTree is ready, add two more records.
$('#jstree').on('ready.jstree', function (e, data) {
    setTimeout(() => {
        createNode("#jstree", "another_base_directory", "Another Base Directory", "first");
        createNode("#base_directory", "sub_2", "Sub 2", "last");
    },2000);
});

// Helper method createNode(parent, id, text, position).
// Dynamically adds nodes to the jsTree. Position can be 'first' or 'last'.
function createNode(parent_node, new_node_id, new_node_text, position) {
	$('#jstree').jstree('create_node', $(parent_node), { "text":new_node_text, "id":new_node_id }, position, false, false);	
}








