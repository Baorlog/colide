var baseURL = "https://r2w1dvj3ue.execute-api.ap-southeast-1.amazonaws.com/dev";
var REDIRECT_URL = "https://colide.xyz/dashboard";
// var REDIRECT_URL = "http://localhost/dashboard";

function getParams(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var value = url.searchParams.get(param);
    return value;
}

function getPastelColor(){ 
    return "hsl(" + 360 * Math.random() + ',' +
               (25 + 70 * Math.random()) + '%,' + 
               (85 + 10 * Math.random()) + '%)'
}

function getTokens(code, callback) {
    // var url = "https://colide.auth.ap-southeast-1.amazoncognito.com/oauth2/token?grant_type=authorization_code&code=" + code + "&client_id=46k07lfq3omb8qetsrknbghcml&redirect_uri=https://colide.xyz/dashboard";
    var url = "https://colide.auth.ap-southeast-1.amazoncognito.com/oauth2/token?grant_type=authorization_code&code=" + code + "&client_id=46k07lfq3omb8qetsrknbghcml&redirect_uri=" + REDIRECT_URL;

    $.ajax({
        url: url,
        type: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(response){
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("accessToken", response["access_token"]);
                localStorage.setItem("refreshToken", response["refresh_token"]);
                localStorage.setItem("idToken", response["id_token"]);
                callback();
            } else {
                alert("Sorry, your browser does not support Web Storage...");
            }
        }
    });
}

function setUserInfo(userInfos) {
    localStorage.setItem("username", userInfos["Username"]);
    $(".user-info-text-username").html("User#" + userInfos["Username"]);
    if(userInfos.hasOwnProperty("UserAttributes")) {
        let userAttributes = {};
        userInfos["UserAttributes"].forEach((attr, idx, arr) => {
            userAttributes[attr["Name"]] = attr["Value"];
            if(idx == arr.length - 1) {
                localStorage.setItem("userAttributes", JSON.stringify(userAttributes));
                localStorage.setItem("editorTheme", userAttributes["custom:editorTheme"]);
                if(userAttributes.hasOwnProperty("name")) {
                    $(".user-info-text-name").html(userAttributes["name"]);
                }
                else {
                    $(".user-info-text-name").html(userInfos["Username"]);
                }
                if(userAttributes.hasOwnProperty("email")) {
                    $(".user-info-text-email span").html(userAttributes["email"]);
                }
                if(userAttributes.hasOwnProperty("picture")) {
                    $(".user-info-avatar-area").html($("<img>", {src:userAttributes["picture"], alt: "avatar", class: "user-info-avatar"}));
                    $(".avatar").attr("src", userAttributes["picture"]);
                }
            }
        })
    }
}

function getUserInfo() {
    $.ajax({
        url: baseURL + "/user/get-own-user-details",
        type: "POST",
        data: JSON.stringify({
            "accessToken": localStorage.getItem("accessToken")
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            // console.log(response);
            setUserInfo(response);
            getWorkingProjects();
            getAssignedIssues();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
            localStorage.removeItem("idToken")
            redirectTo("/");
        }
    });
}

function addNewProject(project) {
    let projectInfoArray = project["SK"].split("#");
    let projectName = projectInfoArray[2];
    let projectNameFirstLetter = projectName[0].toUpperCase();
    let projectCreator = projectInfoArray[1];
    let role = project["Role"];
    let joined = project["Joined"];
    let star = project["Starred"];
    let starSrc = null;
    let starAlt = null;
    if(star) {
        starSrc = "img/star2.png";
        starAlt = "star";
    } else {
        starSrc = "img/unstar2.png";
        starAlt = "unstar";
    }


    // $.ajax({
    //     url: baseURL + "/server/get-server",
    //     type: "GET",
    //     data: {
    //         "projectID": "Project#baolong#project4",
    //         "serverID": "Server#1605683464020"
    //     },
    //     headers: {
    //         "Content-Type": "application/json",
    //         "Authorization": localStorage.getItem("idToken")
    //     },
    //     success: function(response){
    //         console.log(response["InstanceInfo"]["PublicIpAddress"]);
    //         localStorage.setItem("targetPlayground", response["InstanceInfo"]["PublicIpAddress"]);
            $("#project-list").append(
                $("<li>").append([
                    $("<div>", {class: "project-avatar-container col span-1-of-10"}).html(
                        $("<a>", {href: "#", class: "project-avatar-link"}).html(
                            $("<div>", {class: "project-avatar", text: projectNameFirstLetter}).css("background-color", getPastelColor())
                        ).click(function() {
                            localStorage.setItem("currentProjectID", project["SK"]);
                            redirectTo("/project");
                        })
                    ),
                    $("<div>", {class: "project-info-container col span-6-of-10"}).append([
                        $("<div>", {class: "project-info-upper"}).append([
                            $("<a>", {href: "#", class: "project-name-link"}).html(
                                $("<h2>", {text: projectName})
                            ).click(function() {
                                localStorage.setItem("currentProjectID", project["SK"]);
                                redirectTo("/project");
                            }),
                            $("<div>", {class: "project-info-role", text: role})
                        ]),
                        $("<div>", {class: "project-info-lower"}).append([
                            $("<label>", {for: "author", text: "Created by: "}),
                            $("<span>", {id: "project-info-author", text: projectCreator})
                        ])
                    ]),
                    $("<div>", {class: "project-joined-container col span-2-of-10"}).html(
                        $("<div>", {class: "project-joined"}).append([
                            $("<label>", {for: "joined", text: "Joined on "}),
                            $("<span>", {id: "project-info-joined", text: joined})
                        ])
                    ),
                    $("<div>", {class: "project-star-container col span-1-of-10"}).html(
                        $("<div>", {class: "project-star"}).html(
                            $("<img>", {alt: starAlt, src: starSrc}).click(function() {
                                if($(this).attr("src") == "img/unstar2.png") {
                                    $(this).attr("src", "img/star2.png");
                                    starProject(project["SK"]);
                                } else {
                                    $(this).attr("src", "img/unstar2.png");
                                    unstarProject(project["SK"]);
                                }
                            })
                        )
                    )
                ])
            )
    //     },
    //     error: function(XMLHttpRequest, textStatus, errorThrown) {
    //         console.log(XMLHttpRequest);
    //         console.log(textStatus);
    //         console.log(errorThrown);
    //     }
    // });

}

function setWorkingProjects(projectInfos) {
    let projectNumber = projectInfos["Count"];
    if(projectNumber) {
        let projects = projectInfos["Items"];
        projects.forEach((project) => {
            addNewProject(project);
        })
    } else {
        $("#project-list-status").css("display", "block");
    }
}

function getWorkingProjects() {
    $.ajax({
        url: baseURL + "/project/get-working-projects",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            setWorkingProjects(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function addNewIssue(issue) {
    let issueTitle = issue["Title"];
    let issueDeadline = issue["Deadline"];

    $("#issue-list").append(
        $("<li>").append([
            $("<div>", {class: "issue-title col span-4-of-5"}).html(
                $("<a>", {href: "#", class: "issue-title-link", text: issueTitle}).click(function() {
                    localStorage.setItem("currentProjectID", issue["PK"]);
                    localStorage.setItem("targetProjectView", "Issues");
                    redirectTo("/project");
                })
            ),
            $("<div>", {class: "issue-deadline col span-1-of-5", text: issueDeadline})
        ])
    )
}

function setAssignedIssues(issueInfos) {
    let issueNumber = issueInfos["Count"];
    if(issueNumber) {
        let issues = issueInfos["Items"];
        issues.forEach((issue) => {
            addNewIssue(issue);
        })
    } else {
        $("#issue-list-status").css("display", "block");
    }
}

function getAssignedIssues() {
    $.ajax({
        url: baseURL + "/issue/get-assigned-issues",
        type: "GET",
        data: {
            "username": localStorage.getItem("username")
        },
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            setAssignedIssues(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function starProject(projectID) {
    $.ajax({
        url: baseURL + "/project/star-project",
        type: "POST",
        data: JSON.stringify({
            "username": localStorage.getItem("username"),
            "projectID": projectID
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            console.log(response);
            alert("Success");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function unstarProject(projectID) {
    $.ajax({
        url: baseURL + "/project/unstar-project",
        type: "POST",
        data: JSON.stringify({
            "username": localStorage.getItem("username"),
            "projectID": projectID
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("idToken")
        },
        success: function(response){
            console.log(response);
            alert("Success");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function dashboardJobs() {
    getUserInfo();
}

function redirectTo(resource) {
    window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + resource;
}

$(document).ready(function() {
    let isLogout = getParams("logout");
    if(isLogout) {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
        redirectTo("/");
    }
    
    let authenCode = getParams("code");
    if(authenCode) {
        getTokens(getParams("code"), dashboardJobs);
        window.history.pushState({},"", location.protocol + "//" + location.hostname + ":" + location.port + location.pathname)
    }
    else 
        dashboardJobs();

    // New Project
    $("#newProject").click(function() {
        redirectTo("/new-project");
    })

    // Project filter buttons
    $("#yourProjects").click(() => {
        window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + "/dashboard";
    });
    $("#createdProjects").click(() => {
        window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + "/created-projects";
    });
    $("#starredProjects").click(() => {
        window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + "/starred-projects";
    });
    $("#groupProjects").click(() => {
        window.location.href = location.protocol + "//" + location.hostname + ":" + location.port + "/group-projects";
    });

    // Logout button
    $("#logout-btn").click(function() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        localStorage.removeItem("username");
        localStorage.removeItem("userAttributes");
    })
})
