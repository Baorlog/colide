const dirTree = require('directory-tree');
var fs = require('fs');
const AWS = require('aws-sdk');
var path = require("path");

var s3 = new AWS.S3({apiVersion: '2006-03-01'});

function getCodebase() {
    const tree = dirTree('codebase');
    return tree;
}

function getDirectoryTree(path) {
    const tree = dirTree(path);
    return tree;
}

function writeFile(dir, fileName, contents, cb) {
    fs.access(dir, function(err) {
        if (err && err.code === 'ENOENT') {
            console.log(`Directory (${dir}) does not exists\nCreating directory ${dir}`);
            fs.mkdir(dir, {recursive: true}, function() {
                if(fileName != '')
                    fs.writeFile(dir + fileName, contents, cb)
            })
        } else {
            if(fileName != '')
                fs.writeFile(dir + fileName, contents, cb)
        }
      });
}

function deleteFile(filePath, callback) {
    fs.stat(filePath, function (err, stats) {
        if (err && err.code === 'ENOENT') {
            console.log(`File (${filePath}) does not exists.`);
        } else {
            console.log(`Deleting file ${filePath}...`);
        
            fs.unlink(filePath, callback);  
        }
    });
}

function deleteDirectory(directoryPath, callback) {
    fs.stat(directoryPath, function (err, stats) {
        if (err && err.code === 'ENOENT') {
            console.log(`Directory (${directoryPath}) does not exists.`);
        } else {
            console.log(`Deleting directory ${directoryPath}...`);
        
            fs.rmdir(directoryPath, { recursive: true }, callback); 
        }
    });
}

function rename(newPath, oldPath, callback) {
    fs.stat(oldPath, function (err, stats) {
        if (err && err.code === 'ENOENT') {
            console.log(`Object (${oldPath}) does not exists.`);
        } else {
            console.log(`Renaming object ${oldPath} to ${newPath}...`);
        
            fs.rename(oldPath, newPath, callback); 
        }
    });
}

function backSlashToSlash(string) {
    return string.replace(/\\/g, "/");
}

function extractPath(fullPath) {
    // let result = [];
    // let path = "";
    // let pathElements = fullPath.split("/");
    // for(const key in pathElements) {
    //     if(key < pathElements.length - 1) {
    //         path += pathElements[key] + "/";
    //     }
    // }
    // result.push(path);
    // result.push(pathElements[pathElements.length - 1]);
    // return result;
    let pathElements = backSlashToSlash(fullPath).split("/");
    let fileName = pathElements.pop();
    let dir = pathElements.join("/") + "/";
    return [dir, fileName];
}

function downloadCodebaseFromBucket(bucketName, projectName) {
    console.log(`Pulling codebase from S3 bucket (${bucketName}), in project ${projectName}...`)
    var params = {
        Bucket: bucketName,
        // Delimiter: '/',
        Prefix: `${projectName}/`
    };
    s3.listObjectsV2(params, function(err, data) {
        if (err) console.log(err, err.stack); 
        else {
            data.Contents.forEach((fileObj, fileIndex, fileArray) => {
                var fileParams = {
                    Bucket: bucketName, 
                    Key: fileObj.Key
                };
                s3.getObject(fileParams, function(fileErr, fileData){
                    if(fileErr) console.log(fileErr);
                    else {
                        console.log(fileObj.Key);
                        var fileObjKeyArray = fileObj.Key.split("/");
                        var projectName = fileObjKeyArray.shift();
                        var fileObjKey = fileObjKeyArray.join("/");
                        // let filePath = extractPath("codebase/" + fileObj.Key);
                        let filePath = extractPath(fileObj.Key);
                        writeFile(filePath[0], filePath[1], fileData.Body, function(err) {
                            if(err) console.log(err);
                            if(fileIndex === fileArray.length - 1) {
                                console.log(`Pulling codebase (${bucketName}) completed.`);
                            }
                        })
                    }
                })
            })
        }
    });
}

function uploadDir(s3Path, bucketName) {

    function walkSync(currentDirPath, callback) {
        fs.readdirSync(currentDirPath).forEach(function (name) {
            var filePath = path.join(currentDirPath, name);
            var stat = fs.statSync(filePath);
            if (stat.isFile()) {
                callback(filePath, stat);
            } else if (stat.isDirectory()) {
                walkSync(filePath, callback);
            }
        });
    }

    walkSync(s3Path, function(filePath, stat) {
        // let bucketPath = filePath.substring(s3Path.length+1);
        let params = {Bucket: bucketName, Key: filePath, Body: fs.readFileSync(filePath) };
        s3.putObject(params, function(err, data) {
            if (err) {
                console.log(err)
            } else {
                console.log('Successfully backup '+ filePath +' to ' + bucketName);
            }
        });

    });
};

module.exports = {
    getCodebase, getDirectoryTree, writeFile, deleteFile, deleteDirectory, rename, extractPath, downloadCodebaseFromBucket, uploadDir
}