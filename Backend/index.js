const path = require('path');
const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const port = process.env.PORT || 80;

let app = express();
const publicPath = path.join(__dirname, '/../Frontend');

app.use(express.static(path.join(publicPath + "/resources")))
app.use(express.static(path.join(publicPath + "/vendors")))
// app.use(express.static(path.join(publicPath + "/socket.io/socket.io.js")))

app.get("/", (req, res) => {
    res.sendFile(path.join(publicPath + "/index.html"));
})

app.get("/dashboard", (req, res) => {
    res.sendFile(path.join(publicPath + "/dashboard.html"));
})

app.get("/created-projects", (req, res) => {
    res.sendFile(path.join(publicPath + "/created-projects.html"));
})

app.get("/starred-projects", (req, res) => {
    res.sendFile(path.join(publicPath + "/starred-projects.html"));
})

app.get("/group-projects", (req, res) => {
    res.sendFile(path.join(publicPath + "/group-projects.html"));
})

app.get("/project", (req, res) => {
    res.sendFile(path.join(publicPath + "/project.html"));
})

app.get("/new-group", (req, res) => {
    res.sendFile(path.join(publicPath + "/new-group.html"));
})

app.get("/new-project", (req, res) => {
    res.sendFile(path.join(publicPath + "/new-project.html"));
})

app.get("/new-issue", (req, res) => {
    res.sendFile(path.join(publicPath + "/new-issue.html"));
})

app.get("/profile", (req, res) => {
    res.sendFile(path.join(publicPath + "/profile.html"));
})

app.get("/playground", (req, res) => {
    res.sendFile(path.join(publicPath + "/playground.html"));
})

let server = http.createServer(app);
let io = socketIO(server);

// app.listen(port)
server.listen(port, '0.0.0.0', () => {
    console.log(`Server is now serving on port ${port}`);
})

// const http = require('http');
// const socketIO = require('socket.io');
// const fs = require('fs');

// const codebase = require('./codebase');

// const publicPath = path.join(__dirname, '/../Frontend');
// let server = http.createServer(app);
// let io = socketIO(server);

// codebase.downloadCodebaseFromBucket("colide", "Project#thuynga#cvrp");

// app.use(express.static(publicPath));

// io.on('connection', function(socket) {
//     var clientIp = socket.request.connection._peername;
//     var clientInfo = "" + clientIp.address + ":" + clientIp.port;
//     console.log("Client at " + clientInfo + " have just connected.");
    
//     var rand = function(min, max) {
//         if (min==null && max==null)
//             return 0;    
        
//         if (max == null) {
//             max = min;
//             min = 0;
//         }
//         return min + Math.floor(Math.random() * (max - min + 1));
//     };

//     var color = "rgb(" + rand(0,256) + "," + rand(0,256) + "," + rand(0,256) + ")";

//     // When client disconnect
//     socket.on('disconnect', function() {
//         console.log("Client at " + clientIp.address + ":" + clientIp.port + " have just disconnected.");
//         io.emit('ClientLeft', clientInfo);
//     });

//     // When client get the codebase
//     socket.on('GetCodebase', function(projectName) {
//         console.log(projectName);
//         console.log(`Sending codebase information ${projectName} to client ${clientInfo}...`);
//         // socket.emit("GetCodebase", codebase.getCodebase());
//         socket.emit("GetCodebase", codebase.getDirectoryTree(projectName));
//         console.log(`Codebase sent to client ${clientInfo}`);
//     });

//     // When client get the code of a specific file.
//     socket.on('GetFile', function(filePath) {
//         console.log("Client at " + clientInfo + " get file: " + filePath);

//         // Get file extension
//         let separateByDot = filePath.split('.');
//         let fileExtension = separateByDot[separateByDot.length - 1];
        
//         // Read file
//         if(fileExtension == "jpg" || fileExtension == "png" || fileExtension == "jpeg") {
//             fs.readFile(filePath, function(err, data) {
//                 if(err) {
//                     console.log(err);
//                     socket.emit("ServerMessage", "File is not exists or corrupted.");
//                 } else {
//                     socket.emit('GetFile', {
//                         "path": filePath,
//                         "content": "data:image/png;base64,"+ data.toString("base64"),
//                         "image": true
//                     })
//                 }
//             })
//         } else {
//             fs.readFile(filePath, "utf8", function (err, data) {
//                 if(err) {
//                     console.log(err);
//                     socket.emit("ServerMessage", "File is not exists or corrupted.");
//                 } else {
//                     socket.emit('GetFile', {
//                         "path": filePath,
//                         "content": data,
//                         "image": false
//                     });
//                 }
//             })
//         }
//     });

//     // When client join a file
//     socket.on('ClientJoin', function() {

//     });

//     // Information about client's cursor activities
//     socket.on('CMCursor', function(cursorPos) {

//         var cursorInformation = {
//             "client": clientInfo,
//             "position": cursorPos,
//             "color": color
//         };
//         socket.broadcast.emit('CMCursor', cursorInformation);
//     });

//     // Information about client's activities on the editor
//     socket.on('CMEditorChange', function(change) {
//         socket.broadcast.emit('CMEditorChange', change);
//     });

//     // Information about file changes on the server
//     socket.on('FileChange', function(fileChangeInformation) {
//         switch(fileChangeInformation.type) {
//             case "new-file":
//                 var pathElements = codebase.extractPath(fileChangeInformation.path);
//                 fileChangeInformation["directory"] = pathElements[0];
//                 fileChangeInformation["name"] = pathElements[1];
//                 console.log(`Client at ${clientInfo} request to create new file ${fileChangeInformation.path}.`);
//                 codebase.writeFile(fileChangeInformation.directory, fileChangeInformation.name, "", function (err) {
//                     if (err) throw err;
//                     console.log('File is created successfully.');
//                     io.emit("FileChange", fileChangeInformation);
//                 }); 
//                 break;
//             case "new-folder":
//                 console.log(`Client at ${clientInfo} request to create new directory ${fileChangeInformation.path}.`);
//                 codebase.writeFile(fileChangeInformation.path, "", "", function (err) {
//                     if (err) throw err;
//                     console.log('Directory is created successfully.');
//                     io.emit("FileChange", fileChangeInformation);
//                 }); 
//                 break;
//             case "save":
//                 var pathElements = codebase.extractPath(fileChangeInformation.path);
//                 fileChangeInformation["directory"] = pathElements[0];
//                 fileChangeInformation["name"] = pathElements[1];
//                 console.log(`Client at ${clientInfo} request to save file ${fileChangeInformation.path}.`);
//                 codebase.writeFile(fileChangeInformation.directory, fileChangeInformation.name, fileChangeInformation.content, function (err) {
//                     if (err) throw err;
//                     console.log('File is saved successfully.');
//                 }); 
//                 break;
//             case "delete":
//                 if(fileChangeInformation.fileType == "file") {
//                     console.log(`Client at ${clientInfo} request to delete file ${fileChangeInformation.path}.`);
//                     codebase.deleteFile(fileChangeInformation.path, function(err){
//                         if(err) throw err;
//                         console.log(`File ${fileChangeInformation.path} deleted successfully`);
//                         io.emit("FileChange", fileChangeInformation);
//                     });
//                 } else if (fileChangeInformation.fileType == "directory") {
//                     console.log(`Client at ${clientInfo} request to delete directory ${fileChangeInformation.path}.`);
//                     codebase.deleteDirectory(fileChangeInformation.path, function(err) {
//                         if(err) throw err;
//                         console.log(`Directory ${fileChangeInformation.path} deleted successfully`);
//                         io.emit("FileChange", fileChangeInformation);
//                     });
//                 }
//                 break;
//             case "rename":
//                 console.log(`Client at ${clientInfo} request to rename object ${fileChangeInformation.oldPath} to ${fileChangeInformation.newPath}.`);
//                 codebase.rename(fileChangeInformation.newPath, fileChangeInformation.oldPath, function(err) {
//                     if(err) throw err;
//                     console.log(`Successfully rename ${fileChangeInformation.oldPath} to ${fileChangeInformation.newPath}.`);
//                     io.emit("FileChange", fileChangeInformation);
//                 })
//                 break;
//             default:
//                 console.log("Unknown file change information type")
//                 socket.emit('ServerMessage', "Unknown file change information type");
//         }
//     });

//     // When a client want to refresh the codebase tree
//     socket.on("Refresh", function(projectName) {
//         console.log(`Sending codebase information ${projectName} to client ${clientInfo} for refreshing...`);
//         socket.emit("Refresh", codebase.getCodebase());
//         console.log(`Codebase sent to client ${clientInfo}`);
//     })

//     // For broadcast message
//     socket.on('Broadcast', function(broadcastMessage) {
//         console.log("Broadcast from client " + clientInfo + ": " + broadcastMessage);
//         socket.broadcast.emit('Broadcast', broadcastMessage)
//     });
// });

// server.listen(port, '0.0.0.0', () => {
//     console.log(`Server is now serving on port ${port}`);
// })