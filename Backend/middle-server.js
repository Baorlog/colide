console.log("---------------------------------------------");
console.log("Starting up server...");
console.log("---------------------------------------------");

const path = require('path');
const http = require('http');
// const https = require('https');
const express = require('express');
const socketIO = require('socket.io');
const fs = require('fs');
var Client = require("ssh2").Client;

const codebase = require('./codebase');

const publicPath = path.join(__dirname, '/../Frontend');
const port = process.env.PORT || 1503;
let app = express();
// let server = https.createServer({
//     key: fs.readFileSync('./cert/private.key'),
//     cert: fs.readFileSync('./cert/certificate.crt'),
//     ca: fs.readFileSync('./cert/ca_bundle.crt'),
//     requestCert: false,
//     rejectUnauthorized: false
// },app);
let server = http.createServer(app);
let io = socketIO(server);

// codebase.downloadCodebaseFromBucket("colide", "Project#baolong#project");

app.use(express.static(path.join(publicPath + "/resources")));
app.use(express.static(path.join(publicPath + "/vendors")));

app.get("/playground", (req, res) => {
    res.sendFile(path.join(publicPath + "/playground.html"));
})

var activeConnection = {};
var serverProjectName = "";

io.on('connection', function(socket) {
    var clientIp = socket.request.connection._peername;
    var clientInfo = "" + clientIp.address + ":" + clientIp.port;
    console.log("---------------------------------------------");
    console.log("Client at " + clientInfo + " have just connected.");
    console.log("---------------------------------------------");
    
    var rand = function(min, max) {
        if (min==null && max==null)
            return 0;    
        
        if (max == null) {
            max = min;
            min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
    };

    var color = "rgb(" + rand(0,256) + "," + rand(0,256) + "," + rand(0,256) + ")";

    // When client disconnect
    socket.on('disconnect', function() {
        console.log("---------------------------------------------");
        console.log("Client at " + clientIp.address + ":" + clientIp.port + " have just disconnected. Deleting information...");
        delete activeConnection[clientInfo];
        io.emit('ClientLeft', clientInfo);
        if(Object.keys(activeConnection).length == 0 && serverProjectName != "") {
            codebase.uploadDir("./" + serverProjectName, "colide");
        }
        console.log("Information deleted.")
        console.log("---------------------------------------------");
    });

    // When client get the codebase
    socket.on('GetCodebase', function(projectName) {
        console.log("---------------------------------------------");
        if(projectName == "") {
            console.log("client at " + clientInfo + " not specified codebase name. Require client to retry.");
            socket.emit("GetCodebase", "Retry");
            console.log("---------------------------------------------");
        }
        else {
            console.log("Client at " + clientInfo + " requesting codebase " + projectName);
            console.log(`Sending codebase information ${projectName} to client ${clientInfo}...`);
            // socket.emit("GetCodebase", codebase.getCodebase());
            socket.emit("GetCodebase", codebase.getDirectoryTree(projectName));
            console.log(`Codebase sent to client ${clientInfo}`);
            console.log("---------------------------------------------");

            if(serverProjectName == "") {
                serverProjectName = projectName;
            }
        }
    });

    // When client get the code of a specific file.
    socket.on('GetFile', function(filePath) {
        console.log("---------------------------------------------");
        console.log("Client at " + clientInfo + " get file: " + filePath);

        // Get file extension
        let separateByDot = filePath.split('.');
        let fileExtension = separateByDot[separateByDot.length - 1];
        
        // Read file
        if(fileExtension == "jpg" || fileExtension == "png" || fileExtension == "jpeg") {
            fs.readFile(filePath, function(err, data) {
                if(err) {
                    console.log(err);
                    socket.emit("ServerMessage", "File is not exists or corrupted.");
                } else {
                    socket.emit('GetFile', {
                        "path": filePath,
                        "content": "data:image/png;base64,"+ data.toString("base64"),
                        "image": true
                    })
                }
            })
        } else {
            fs.readFile(filePath, "utf8", function (err, data) {
                if(err) {
                    console.log(err);
                    socket.emit("ServerMessage", "File is not exists or corrupted.");
                } else {
                    socket.emit('GetFile', {
                        "path": filePath,
                        "content": data,
                        "image": false
                    });
                }
            })
        }
        console.log("---------------------------------------------");
    });

    // When client join a file
    socket.on('ClientJoin', function(username) {
        console.log("---------------------------------------------");
        console.log("User " + username + " has joined.");
        if(username == null) {
            console.log("User has no valid information. Disconnected.");
            socket.disconnect();
        }
        else {
            socket.broadcast.emit("ClientJoin", {
                "exists": false,
                "username": username,
                "color": color,
                "connect": clientInfo
            });
            Object.keys(activeConnection).forEach(connection => {
                activeConnection[connection]["exists"] = true;
                socket.emit("ClientJoin", activeConnection[connection]);
            })
            activeConnection[clientInfo] = {
                "username": username,
                "color": color,
                "connect": clientInfo
            };
        }
        console.log("---------------------------------------------");
    });

    // Information about client's cursor activities
    socket.on('CMCursor', function(cursorPosInfo) {

        var cursorInformation = {
            "client": clientInfo,
            "position": cursorPosInfo.cursorPos,
            "color": color,
            "currentFilePath": cursorPosInfo.currentFilePath
        };
        socket.broadcast.emit('CMCursor', cursorInformation);
    });

    // Information about client's activities on the editor
    socket.on('CMEditorChange', function(change) {
        socket.broadcast.emit('CMEditorChange', change);
    });

    // Information about file changes on the server
    socket.on('FileChange', function(fileChangeInformation) {
        console.log("---------------------------------------------");
        switch(fileChangeInformation.type) {
            case "new-file":
                var pathElements = codebase.extractPath(fileChangeInformation.path);
                fileChangeInformation["directory"] = pathElements[0];
                fileChangeInformation["name"] = pathElements[1];
                console.log(`Client at ${clientInfo} request to create new file ${fileChangeInformation.path}.`);
                codebase.writeFile(fileChangeInformation.directory, fileChangeInformation.name, "", function (err) {
                    if (err) throw err;
                    console.log('File is created successfully.');
                    io.emit("FileChange", fileChangeInformation);
                }); 
                break;
            case "new-folder":
                console.log(`Client at ${clientInfo} request to create new directory ${fileChangeInformation.path}.`);
                codebase.writeFile(fileChangeInformation.path, "", "", function (err) {
                    if (err) throw err;
                    console.log('Directory is created successfully.');
                    io.emit("FileChange", fileChangeInformation);
                }); 
                break;
            case "save":
                var pathElements = codebase.extractPath(fileChangeInformation.path);
                fileChangeInformation["directory"] = pathElements[0];
                fileChangeInformation["name"] = pathElements[1];
                console.log(`Client at ${clientInfo} request to save file ${fileChangeInformation.path}.`);
                codebase.writeFile(fileChangeInformation.directory, fileChangeInformation.name, fileChangeInformation.content, function (err) {
                    if (err) throw err;
                    console.log('File is saved successfully.');
                }); 
                break;
            case "delete":
                if(fileChangeInformation.fileType == "file") {
                    console.log(`Client at ${clientInfo} request to delete file ${fileChangeInformation.path}.`);
                    codebase.deleteFile(fileChangeInformation.path, function(err){
                        if(err) throw err;
                        console.log(`File ${fileChangeInformation.path} deleted successfully`);
                        io.emit("FileChange", fileChangeInformation);
                    });
                } else if (fileChangeInformation.fileType == "directory") {
                    console.log(`Client at ${clientInfo} request to delete directory ${fileChangeInformation.path}.`);
                    codebase.deleteDirectory(fileChangeInformation.path, function(err) {
                        if(err) throw err;
                        console.log(`Directory ${fileChangeInformation.path} deleted successfully`);
                        io.emit("FileChange", fileChangeInformation);
                    });
                }
                break;
            case "rename":
                console.log(`Client at ${clientInfo} request to rename object ${fileChangeInformation.oldPath} to ${fileChangeInformation.newPath}.`);
                codebase.rename(fileChangeInformation.newPath, fileChangeInformation.oldPath, function(err) {
                    if(err) throw err;
                    console.log(`Successfully rename ${fileChangeInformation.oldPath} to ${fileChangeInformation.newPath}.`);
                    io.emit("FileChange", fileChangeInformation);
                })
                break;
            default:
                console.log("Unknown file change information type")
                socket.emit('ServerMessage', "Unknown file change information type");
        }
        console.log("---------------------------------------------");
    });

    // When a client want to refresh the codebase tree
    socket.on("Refresh", function(projectName) {
        console.log("---------------------------------------------");
        console.log(`Sending codebase information ${projectName} to client ${clientInfo} for refreshing...`);
        socket.emit("Refresh", codebase.getDirectoryTree(projectName));
        console.log(`Codebase sent to client ${clientInfo}`);
        console.log("---------------------------------------------");
    })

    // When a client use Terminal
    socket.on("Terminal-init", function() {
        console.log("---------------------------------------------");
        console.log("Client at " + clientInfo + " init a new SSH session.");
        var conn = new Client();
        conn.on('ready', function() {
            console.log('SSH session for client ' + clientInfo + " ready.");
            console.log("---------------------------------------------");
            conn.shell(function(err, stream) {
                if (err) throw err;
    
                stream.on('close', function() {
                    socket.emit("Server", "Terminal close");
                    conn.end();
                    console.log("---------------------------------------------");
                    console.log("Client at " + clientInfo + " closed terminal.")
                    console.log("---------------------------------------------");
                }).stderr.on('data', function(data) {
                    socket.emit("Server", data);
                });
    
                stream.stdout.on("data", (data) => {
                    socket.emit("Terminal", data.toString("binary"));
                })
    
    
                socket.on("Terminal", function(input) {
                    stream.stdin.write(input);
                });
                
    
                socket.on("Terminal-resize", function(resizeInfo) {
                    stream.setWindow(resizeInfo.row, resizeInfo.col);
                })
    
            });
        }).connect({
            host: "127.0.0.1",
            port: 22,
            // username: "superuna",
            // password: "kien1503",
            username: "ubuntu",
            privateKey: fs.readFileSync("Colide.pem").toString("utf-8")
        });
    })

    // When a client use chat
    socket.on("Message", function(chatInfo) {
        console.log("---------------------------------------------");
        chatInfo["color"] = color;
        console.log("Received message from " + clientInfo + ": ");
        console.log(chatInfo);
        io.emit("Message", chatInfo);
        console.log("---------------------------------------------");
    })

    // For broadcast message
    socket.on('Broadcast', function(broadcastMessage) {
        console.log("---------------------------------------------");
        console.log("Broadcast from client " + clientInfo + ": " + broadcastMessage);
        socket.broadcast.emit('Broadcast', broadcastMessage);
        console.log("---------------------------------------------");
    });
});

server.listen(port, '0.0.0.0', () => {
    console.log("---------------------------------------------");
    console.log(`Server is now serving on port ${port}`);
    console.log("---------------------------------------------");
})